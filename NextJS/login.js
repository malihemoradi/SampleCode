import React, {useState} from 'react';
import styles from '../../styles/Login.module.scss';
import TextField from "@material-ui/core/TextField";
import {MuiThemeProvider, createMuiTheme, StylesProvider} from "@material-ui/core/styles";
import {create} from 'jss';
import jssRTL from 'jss-rtl';
import {jssPreset, createGenerateClassName} from '@material-ui/core/styles';
import PageAppBar from "../../components/PageAppBar";
import API from '../../help/API';
import Router from 'next/router';
import ProgressButton from "../../components/ProgressButton";
import {convertFaNumberToEn} from "../../help/Helper";
import {isValidPhoneNumber} from "../../help/Validator";
import Messages from "../../help/Messages";
import Cookies from 'universal-cookie';
import {VERIFY_PAGE} from "../../help/Path";
import PageBottomBar from "../../components/PageBottomBar";

const cookies = new Cookies();
const current = new Date();
const nextYear = new Date();

// Configure JSS
const jss = create({plugins: [...jssPreset().plugins, jssRTL()]});

const theme = createMuiTheme({
    direction: 'rtl',
    typography: {
        fontFamily: ['Vazir', 'Open Sans'].join(','),
    },
    palette: {
        primary: {
            main: styles.primaryColor
        },
        secondary: {
            main: "#ffcc80" //Another orange-ish color
        }
    },
});

// Custom Material-UI class name generator.
const generateClassName = createGenerateClassName();

export default function Login(props) {
    const [phoneNumber, setPhoneNumber] = useState();
    const [phoneNumberError, setPhoneNumberError] = useState();
    const [phoneNumberErrorVisible, setPhoneNumberErrorVisible] = useState(false);
    const [loading, setLoadingVisibility] = useState(false);


    function _onChangePhoneNumber(e) {
        setPhoneNumber();

        //Convert Fa number to En number
        let phoneNumberEn = convertFaNumberToEn(e.target.value);
        setPhoneNumber(phoneNumberEn);

        if (e.target.value.length === 11) {
            hidePhoneNumberError();
        }
    }

    function onClickSend(event) {
        event.preventDefault();

        //Check validation
        if (!isValidPhoneNumber(phoneNumber)) {
            showPhoneNumberError();
            return;
        }

        //Start loading
        setLoadingVisibility(true);


        API.post('/lawyer/register', {
            phoneNumber: phoneNumber
        }).then(function (res) {
            console.log(res);

            //Stop loading
            setLoadingVisibility(false)

            const lawyerInfo = res.data.result;

            //Save token
            nextYear.setFullYear(current.getFullYear() + 2);
            cookies.set("token", lawyerInfo.token, {
                expires: nextYear
            });

            if (res.data.success) {
                Router.replace({
                    pathname: VERIFY_PAGE,
                    state: {phoneNumber: phoneNumber}
                });
            } else {
                //Show Error
                Messages.showErrorMessage(res.data.message);
            }

        }).catch(function (err) {
            console.log(err);

            //Stop loading
            setLoadingVisibility(false);
            Messages.showErrorMessage("خطای ارتباط با سرور");
        });
    }

    function showPhoneNumberError() {
        setPhoneNumberErrorVisible(true);
        setPhoneNumberError("شماره تلفن را صحیح وارد کنید");
    }

    function hidePhoneNumberError() {
        setPhoneNumberErrorVisible(false);
        setPhoneNumberError("");
    }


    return (
        <MuiThemeProvider theme={theme}>
            {/*Change direction to rtl*/}
            <StylesProvider jss={jss} generateClassName={generateClassName}>
                <div className={styles.root}>
                    <PageAppBar title="ورود وکیل"/>
                    <div className={styles.body}>
                        <form noValidate autoComplete="off" className={styles.form}>
                            <h1 className={styles.introduce}>خوش آمدید!</h1>
                            <p className={styles.description}>برای ورود شماره موبایل خود را وارد کنید</p>
                            <TextField
                                id="phoneNumber"
                                label="شماره تلفن"
                                variant="outlined"
                                placeholder="09xxxxxxxxx"
                                inputProps={{
                                    maxLength: 11,
                                    style: {textAlign: 'left', cursor: 'none', textSize: "36px"}
                                }}
                                className={styles.input}
                                margin="normal"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                onChange={_onChangePhoneNumber}
                                error={phoneNumberErrorVisible}
                                helperText={phoneNumberError}
                            />
                            <div className={styles.sendSection}>
                                <ProgressButton
                                    text="ورود"
                                    loading={loading}
                                    handleButtonClick={event => onClickSend(event)}
                                />
                            </div>
                        </form>
                    </div>
                    <PageBottomBar/>
                </div>
            </StylesProvider>
        </MuiThemeProvider>
    );

}



