import React from "react";
import styles from '../styles/Main.module.scss';
import MainAppBar from "../components/MainAppBar";
import BottomBar from "../components/MainBottomBar";
import ScaleContent from "../components/ScaleContent";
import {createGenerateClassName, createMuiTheme, jssPreset, MuiThemeProvider, StylesProvider} from "@material-ui/core";
import {create} from "jss";
import jssRTL from "jss-rtl";
import MessageBox from "../components/MessageBox";
import SectionCarousel from "../components/SectionCarousel";

// Configure JSS
const jss = create({plugins: [...jssPreset().plugins, jssRTL()]});

const theme = createMuiTheme({
    direction: 'rtl',
    typography: {
        fontFamily: ['Vazir', 'Open Sans'].join(','),
    },
    palette: {
        primary: {
            main: styles.primaryColor
        },
        secondary: {
            main: styles.accentColor
        }
    },
});

// Custom Material-UI class name generator.
const generateClassName = createGenerateClassName();


export default function MainPage() {

    return (
        <MuiThemeProvider theme={theme}>
            {/*Change direction to rtl*/}
            <StylesProvider jss={jss} generateClassName={generateClassName}>
                <div className={styles.container}>
                    <div className={styles.root}>
                        <section id="AppBar">
                            <MainAppBar/>
                        </section>
                        <section className={styles.body} id="body">
                            <main className={styles.main}>
                                <section id="rightSection" className={styles.rightSection}>
                                    <SectionCarousel/>
                                </section>
                                <section id="rightSection" className={styles.leftSection}>
                                    <ScaleContent/>
                                </section>
                            </main>
                            <section className={styles.messageSection}>
                                <MessageBox/>
                            </section>
                        </section>
                        <section id="bottomBar">
                            <BottomBar/>
                        </section>
                    </div>
                </div>
            </StylesProvider>
        </MuiThemeProvider>
    )
}
