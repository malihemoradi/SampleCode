import React from 'react';
import styles from '../../styles/Document.module.scss';
import {MuiThemeProvider, createMuiTheme, StylesProvider} from "@material-ui/core/styles";
import {create} from 'jss';
import jssRTL from 'jss-rtl';
import {jssPreset, createGenerateClassName} from '@material-ui/core/styles';
import PageAppBar from "../../components/PageAppBar";
import API from "../../help/API";
import Router from 'next/router';
import {AppContext, AppWrapper} from "../../components/UserContext";
import ProgressButton from "../../components/ProgressButton";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button"
import Cookies from "universal-cookie";

const cookies = new Cookies();
import jwt from "jsonwebtoken";
import CheckRegistrationProcess from "../../help/CheckRegistrationProcess";
import {TERMSOFSERVICE_PAGE} from "../../help/Path";
import PageBottomBar from "../../components/PageBottomBar";

// Configure JSS
const jss = create({plugins: [...jssPreset().plugins, jssRTL()]});

const theme = createMuiTheme({
    direction: 'rtl',
    typography: {
        fontFamily: ['Vazir', 'Open Sans'].join(','),
    },
    palette: {
        primary: {
            main: styles.primaryColor
        },
        secondary: {
            main: "#ffcc80"
        }
    }
});

const boxProps = {
    borderColor: '#aeaeae',
    m: 1,
    border: 1
};

const defaultProps = {
    bgcolor: 'background.paper',
    borderColor: '#aeaeae',
    m: 1,
    border: 1
};

// Custom Material-UI class name generator.
const generateClassName = createGenerateClassName();

class Documents extends React.Component {

    static contextType = AppContext;

    constructor(props, context) {
        super(props, context);
        this.state = {
            errorOfNationalCardPhotoVisible: false,
            errorOfDocumentPhotoVisible: false,
            nationalCardPhoto: null,
            documentPhoto: null
        };
    }

    async componentDidMount() {

        CheckRegistrationProcess().then(lawyerInfo => {
            this.setState({lawyerInfo: lawyerInfo});
        });
    }

    // Upload to local seaweedFS instance
    uploadImage = async file => {
        const formData = new FormData();
        formData.append('file', file);
    };

    onClickSend = (event) => {
        event.preventDefault();

        //Check validation
        if (this.state.nationalCardPhoto == null) {
            this.showErrorOfNationalCardPhoto();
            return;
        }
        if (this.state.documentPhoto == null) {
            this.showErrorOfDocumentPhoto();
            return;
        }


        // Start loading
        this.setState({loading: true});

        const token = jwt.decode(cookies.get("token"), {complete: true}).payload;


        const nationalCard = new File([this.state.nationalCardPhoto], ".jpg");
        const document = new File([this.state.documentPhoto], ".jpg");

        let data = new FormData();
        data.append('phoneNumber', token.phoneNumber)
        data.append('nationalCard', nationalCard);
        data.append('document', document);


        let axiosConfig = {
            header: {
                'Content-Type': 'multipart/form-data'
            }
        }
        const self = this;
        API.post('/file/uploadLawyerDocuments', data, axiosConfig)
            .then(function (res) {
                console.log(res);

                //Stop loading
                self.setState({loading: false});

                if (res.data.success) {
                    Router.replace(TERMSOFSERVICE_PAGE);
                }
            }).catch(function (err) {
            console.log(err);

            //Stop loading
            self.setState({loading: false});
        });
    };

    showErrorOfNationalCardPhoto = () => {
        this.setState({
            errorOfNationalCardPhotoVisible: true,
            errorOfNationalCardPhoto: "تصویر کارت ملی را انتخاب کنید"
        });
    };

    hideErrorOfNationalCardPhoto = () => {
        this.setState({
            errorOfNationalCardPhotoVisible: false,
            errorOfNationalCardPhoto: ""
        });
    };

    showErrorOfDocumentPhoto = () => {
        this.setState({
            errorOfDocumentPhotoVisible: true,
            errorOfDocumentPhoto: "تصویر مدرک تحصیلی را انتخاب کنید"
        });
    };

    hideErrorOfDocumentPhoto = () => {
        this.setState({
            errorOfDocumentPhotoVisible: false,
            errorOfDocumentPhoto: ""
        });
    };

    saveNationalCardPicture = (capturedPhoto, capturedPhotoUrl) => {
        this.setState({
            nationalCardPhoto: capturedPhoto,
            nationalCardPhotoUrl: capturedPhotoUrl
        })

        this.hideErrorOfNationalCardPhoto();
    };

    saveDocumentPicture = (capturedPhoto, capturedPhotoUrl) => {
        this.setState({
            documentPhoto: capturedPhoto,
            documentPhotoUrl: capturedPhotoUrl
        })

        this.hideErrorOfDocumentPhoto();
    };

    handleItemClick = (index) => {

        let categories = this.props.categories;

        if (categories[index].selected === true) {
            categories[index].selected = false

            //Remove from selected array
            this.setState({
                selectedCategories: this.state.selectedCategories.pop({name: categories[index].name})
            });

        } else {
            categories[index].selected = true

            //Add to selected array
            this.setState({
                selectedCategories: this.state.selectedCategories.push(categories[index])
            });

            //Hide error
            this.hideErrorOfCategory();
        }

        this.setState({
            categories: categories
        });
    }

    captureNationalCardPhoto = (event) => {
        const url = event.target.value
        let ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
        if (event.target.files && event.target.files[0] && (ext === "png" || ext === "jpeg" || ext === "jpg")) {
            const blobUrl = URL.createObjectURL(event.target.files[0]);
            this.saveNationalCardPicture(event.target.files[0], blobUrl);
        }
    }

    captureDocumentPhoto = (event) => {
        const url = event.target.value
        let ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
        if (event.target.files && event.target.files[0] && (ext === "png" || ext === "jpeg" || ext === "jpg")) {
            const blobUrl = URL.createObjectURL(event.target.files[0]);
            this.saveDocumentPicture(event.target.files[0], blobUrl);
        }
    }

    render() {
        return (
            <AppWrapper value={this.state}>
                <MuiThemeProvider theme={theme}>
                    {/*Change direction to rtl*/}
                    <StylesProvider jss={jss} generateClassName={generateClassName}>
                        <div className={styles.root}>
                            <PageAppBar title="ارسال مدارک شناسایی"/>
                            <div className={styles.documents}>
                                <Box className={styles.box} display="flex" justifyContent="center"
                                     borderRadius={10} {...boxProps}>
                                    <div className={styles.rightSection}>
                                        <Typography variant="p">آپلود تصویر کارت ملی</Typography>
                                        <input accept="image/*" id="btn-take-nationalCard" type="file"
                                               onChange={e => this.captureNationalCardPhoto(e)}
                                               style={{display: "none"}}/>
                                        <label htmlFor="btn-take-nationalCard">
                                            <Box className={styles.nationalCard} display="flex"
                                                 justifyContent="center"
                                                 borders="2" borderRadius={10} boxShadow={8} {...defaultProps}>
                                                <Button color="primary" aria-label="upload picture" component="span"
                                                        className={styles.cameraBtn}>
                                                    <img
                                                        alt="National Card Image"
                                                        src={this.state.nationalCardPhotoUrl ? this.state.nationalCardPhotoUrl : require("../../assets/images/bg_nationalCard.svg")}
                                                        width="100%" height="100%" className={styles.image}/>
                                                </Button>
                                            </Box>
                                        </label>
                                        <Typography variant="caption" className={styles.help}>
                                            <span>                       <img alt="info"
                                                                              src={require("../../assets/icons/ic_info.svg")}
                                                                              width={16} height={16}
                                                                              style={{
                                                                                  marginLeft: "4px",
                                                                                  marginRight: "4px"
                                                                              }}/></span>
                                            برای گرفتن تصویر روی کادر کلیک کنید و سپس کارت ملی را در فاصله مناسب
                                            روبروی
                                            دوربین قرار دهید بصورتی که تصویر در کادر قرار بگیرد.</Typography>
                                    </div>
                                    <div className={styles.leftSection}>
                                        <Typography variant="p">آپلود تصویر پروانه وکالت یا آخرین مدرک
                                            حقوقی</Typography>
                                        <input accept="image/*" id="btn-take-document" type="file"
                                               onChange={e => this.captureDocumentPhoto(e)}
                                               style={{display: "none"}}/>
                                        <label htmlFor="btn-take-document">
                                            <Box className={styles.document}
                                                 borders="2" borderRadius={10} boxShadow={8}  {...defaultProps}>
                                                <Button color="primary" aria-label="upload picture" component="span"
                                                        className={styles.cameraBtn}>
                                                    <img
                                                        alt="Document Image"
                                                        src={this.state.documentPhotoUrl ? this.state.documentPhotoUrl : require("../../assets/images/bg_document.svg")}
                                                        width="100%" height="100%" className={styles.image}/>
                                                </Button>
                                            </Box>
                                        </label>
                                        <Typography variant="caption" className={styles.help}>
                                            <span>                       <img alt="info"
                                                                              src={require("../../assets/icons/ic_info.svg")}
                                                                              width={16} height={16}
                                                                              style={{
                                                                                  marginLeft: "4px",
                                                                                  marginRight: "4px"
                                                                              }}/></span>
                                            برای گرفتن تصویر روی کادر کلیک کنید و سپس مدرک را در فاصله مناسب روبروی
                                            دوربین
                                            قرار دهید بصورتی که تصویر در کادر قرار بگیرد.</Typography>
                                    </div>
                                </Box>
                                <div className={styles.sendSection}>
                                    <div className={styles.errorMessage}>
                                        <span>{this.state.errorOfNationalCardPhotoVisible ? this.state.errorOfNationalCardPhoto : ""}</span>
                                        <span>{this.state.errorOfDocumentPhotoVisible ? this.state.errorOfDocumentPhoto : ""}</span>
                                    </div>
                                    <ProgressButton
                                        text="ارسال"
                                        loading={this.state.loading}
                                        handleButtonClick={event => this.onClickSend(event)}
                                    />
                                </div>
                            </div>
                            <PageBottomBar/>
                        </div>
                    </StylesProvider>
                </MuiThemeProvider>
            </AppWrapper>
        );
    }
}

export default Documents;
