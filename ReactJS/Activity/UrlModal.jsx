import React, {Fragment} from "react";
import {Button, Modal, Row, Col, message} from "antd";
import CopyToClipBoard from "react-copy-to-clipboard";
import {FaCopy} from "react-icons/fa";

class UrlModal extends React.Component {

    onCancel = () => {
        this.props.onCancel();
    };

    renderView() {

        if (!this.props.modalVisibility) return;

        return (<Fragment>
            <CopyToClipBoard
                text={this.props.action.url}
                onCopy={() => message.success("Successfully copied")}
            >
                <a
                    href=""
                    title="Copy to Clip board"
                    onClick={e => e.preventDefault()}
                >
                    <FaCopy color="#66BB6A"/>
                </a>
            </CopyToClipBoard>{" "}
            &nbsp;
            <a href={this.props.action.url} target="_blank">{this.props.action.url}</a>
            &nbsp;
            <br/>
            <br/>
            <hr size={1} style={{margin: "0px"}}/>
            <br/>
            <p>
                {this.props.action.message}
            </p>
        </Fragment>);
    }

    render() {
        return (
            <Modal
                visible={this.props.modalVisibility}
                onCancel={this.onCancel}
                title="Detials"
                width={700}
                footer={[
                    <Button type="danger" key="cancel" onClick={this.onCancel}>
                        Cancel
                    </Button>
                ]}
            >
                <Row>
                    <Col>
                        {this.renderView()}
                    </Col>
                </Row>
            </Modal>
        );
    }
}

export default UrlModal;
