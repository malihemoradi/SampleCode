import React, {Fragment} from "react";
import {Button, Modal, Row, Col, message} from "antd";

class LogDetailModal extends React.Component {

    onCancel = () => {
        this.props.onCancel();
    };

    renderView() {

        if (!this.props.modalVisibility) return;

        return (
            <Fragment>
                <p>
                    {this.props.detail}
                </p>
            </Fragment>);
    }

    render() {
        return (
            <Modal
                visible={this.props.modalVisibility}
                onCancel={this.onCancel}
                title="Log Detail"
                width={700}
                footer={[
                    <Button type="danger" key="cancel" onClick={this.onCancel}>
                        Cancel
                    </Button>
                ]}
            >
                <Row>
                    <Col>
                        {this.renderView()}
                    </Col>
                </Row>
            </Modal>
        );
    }
}

export default LogDetailModal;