import React, {Fragment} from "react";
import {Row, Col, Table, Card, Input, Button, DatePicker, Tooltip} from "antd";
import {FaUser} from "react-icons/fa";
import MemberInfoModal from "./MemberInfoModal";
import axios from "axios";
import {ACTIVITY_GET_ALL, App_INFO, MEMBER_GET} from "../Store/API/Address";
import axiosConfig from "../Store/API/AxiosConfig";
import {failedLoadingBar, successLoadingBar} from "../Store/Actions/loadingBarActions";
import {BiError} from "react-icons/bi";
import {VscInfo} from "react-icons/vsc";
import UrlModal from "./UrlModal";
import jalaliMoment from "jalali-moment";

const {RangePicker} = DatePicker;

class ActivityList extends React.Component {

    state = {
        requestActivities: {},
        prevRequestActivities: {},
    };

    constructor(props, context) {
        super(props, context);
        this.state = {
            memberInfoModalVisibility: false,
            urlModalVisibility: false,
            randomKey: Math.round(Math.random() * 1000).toString(),
            memberId: "",
            phoneNumber: "",
            action: "",
            description: "",
            createdAt: "",
            pagination: {
                current: 1,
                pageSize: 10,
            }
        }
    }

    static getDerivedStateFromProps(props, state) {
        if (props.requestActivities !== state.prevRequestActivities) {
            return {
                requestActivities: props.requestActivities,
                prevRequestActivities: props.requestActivities
            };
        }
        return null;
    }

    columns = [
        {
            title: 'Status',
            dataIndex: 'status',
            key: 'status',
            width: '4%',
            render: (text, record) => (
                <Fragment>
                    {(() => {
                        if (text === "error") {
                            return (
                                <BiError size={20}
                                         color="#FF6393"
                                         title="error"/>
                            )
                        } else if (text === "warning") {
                            return (
                                <BiError size={20}
                                         color="#BBA666FF"
                                         title="warning"/>
                            )
                        } else if (text === "info") {
                            return (
                                <VscInfo size={20}
                                         color="#66BB6A"
                                         title="info"/>
                            )
                        }
                    })()}
                </Fragment>)
        },
        {
            title: 'Created At',
            dataIndex: 'createdAt',
            key: 'createdAt',
            width: '13%',
            render: (text, record) => (
                <Tooltip title={
                    jalaliMoment(text).locale('fa').format('YYYY/MM/DD')
                }><span>{text}</span></Tooltip>
            )
        },
        {
            title: 'Action',
            dataIndex: 'title',
            key: 'title',
            width: '10%',
        },
        {
            title: 'Description',
            dataIndex: 'message',
            key: 'message',
            width: '25%',
            ellipsis: true,
            render: (text, record) => (
                <div>{text}</div>
            )
        },
        {
            title: '',
            dataIndex: 'url',
            key: 'url',
            width: '10%',
            render: (text, record) => (
                <div>
                    <Button
                        shape="round"
                        title="Read More"
                        type={"primary"}
                        onClick={e => this.showUrlModal(record)}>
                        &nbsp; Read More...
                    </Button>
                </div>
            )
        },
        {
            title: 'Member',
            dataIndex: 'memberId',
            key: 'memberId',
            render: (text, record) => (
                <a onClick={e => this.showMemberInfoModal(record.memberId)}>
                    <FaUser color="#66BB6A"/>
                    &nbsp;
                    {text}
                </a>
            )
        },
        {
            title: 'Mobile',
            dataIndex: 'phoneNumber',
            key: 'phoneNumber'
        }];


    typoExpand = (record) => {

        this.showLogDetailModal(record).then(r => r);

        this.setState({
            expand: false,
            counter: !this.state.expand
                ? this.state.counter + 0
                : this.state.counter + 1
        });
    };

    typoClose = () => {
        this.setState({
            expand: false,
            counter: !this.state.expand
                ? this.state.counter + 0
                : this.state.counter + 1
        });
    };

    getMemberInfo = async (memberId) => {

        await axios.post(MEMBER_GET, {memberId}, axiosConfig())
            .then(result => {
                if (result.data.ok === 1) {
                    successLoadingBar();
                    this.setState({
                        member: result.data.res,
                        memberInfoModalVisibility: true,
                        randomKey: Math.round(Math.random() * 1000).toString()
                    });

                } else if (result.data.ok === 0) {
                    failedLoadingBar();
                    console.log(result.data.err);
                }
            })
            .catch(err => {
                failedLoadingBar();
                console.log(err);
            });
    };

    closeMemberInfoModal = () => {
        this.setState({memberInfoModalVisibility: false})
    };

    showMemberInfoModal = (memberId) => {
        this.getMemberInfo(memberId);
    };

    showUrlModal = async (record) => {

        this.setState({
            urlModalVisibility: true,
            action: record
        });
    };

    closeUrlModal = () => {
        this.setState({urlModalVisibility: false});
    };

    renderFilterView() {
        return (
            <Card bodyStyle={{padding: "10px"}}>
                <Row>
                    <Col span={5} style={{margin: "5px"}}>
                        <Input
                            type="text"
                            addonBefore="Member Id: "
                            style={{color: "#555"}}
                            onChange={this.onMemberIdChange}
                        />
                    </Col>
                    <Col span={4} style={{margin: "5px"}}>
                        <Input
                            type="text"
                            addonBefore="Mobile: "
                            onChange={this.onMobileNumberChange}
                        />
                    </Col>
                    <Col span={4} style={{margin: "5px"}}>
                        <Input
                            type="text"
                            addonBefore="Action: "
                            onChange={this.onActionChange}
                        />
                    </Col>
                    <Col span={5} style={{margin: "5px"}}>
                        <Input
                            type="text"
                            addonBefore="Description: "
                            onChange={this.onDescriptionChange}
                        />
                    </Col>
                    <Col span={5} style={{margin: "5px"}}>
                        <div style={{
                            background: "#212121",
                            borderRadius: "4px"
                        }}>
                            <span style={{margin: "5px"}}>Created At: </span>
                            <RangePicker
                                onChange={this.onCreatedAtChange}/>
                        </div>
                    </Col>
                    <Col span={3} style={{margin: "5px"}}>
                        <Button
                            type="primary"
                            onClick={this.handleSearchButton}>Search</Button>
                    </Col>
                </Row>
            </Card>
        );
    }

    handleSearchButton = () => {

        if (this.state.memberId.length === 0
            && this.state.phoneNumber.length === 0
            && this.state.action.length === 0
            && this.state.description.length === 0
            && this.state.createdAt.length === 0) {
            return;
        }

        this.search({
            pagination: {
                current: 1,
                pageSize: 10
            }
        });
    };

    search = (params = {}) => {
        const {memberId, phoneNumber, action, description, createdAt} = this.state;
        const {current, pageSize} = (params.pagination) ? params.pagination : this.state.pagination;

        this.setState({
            loading: true,
            pagination: {
                current: current,
                pageSize: pageSize
            }
        });

        const query = {
            "memberId": memberId,
            "phoneNumber": phoneNumber,
            "action": action,
            "description": description,
            "createdAt": createdAt,
            "pageIndex": current,
            "pageSize": pageSize
        };
        this.getActivities(query);
    };

    getActivities = query => {
        axios.post(ACTIVITY_GET_ALL, query, axiosConfig())
            .then(result => {
                if (result.data.ok === 1) {
                    successLoadingBar();
                    this.setState({
                        loading: false,
                        requestActivities: result.data.res
                    })

                } else if (result.data.ok === 0) {
                    failedLoadingBar();
                    console.log(result.data.err);
                }
            })
            .catch(err => {
                failedLoadingBar();
                console.log(err);
            });
    };

    onMemberIdChange = (e) => {
        const {value} = e.target;
        this.setState({memberId: value.trim().toLowerCase()});
    };

    onMobileNumberChange = (e) => {
        const {value} = e.target;
        this.setState({phoneNumber: value});
    };
    onActionChange = (e) => {
        const {value} = e.target;
        this.setState({action: value});
    };
    onDescriptionChange = (e) => {
        const {value} = e.target;
        this.setState({description: value});
    };
    onCreatedAtChange = (date, dateString) => {
        this.setState({createdAt: dateString});
    };

    renderList() {
        return <Fragment>
            <Row>
                <Col style={{margin: "15px"}}>
                    <Table
                        dataSource={this.state.requestActivities}
                        columns={this.columns}
                        showHeader={true}
                        rowKey={record => record._id}
                        locale={{emptyText: "No Route"}}
                        loading={this.state.loading}
                        pagination={{
                            ...this.state.pagination,
                            showQuickJumper: true,
                            total: 500,
                            onChange: (page, size) => {
                                this.search({
                                    loading: true,
                                    pagination: {
                                        current: page,
                                        pageSize: size
                                    }
                                });
                            }
                        }}

                    />
                    <UrlModal
                        key={this.state.randomKey}
                        {...this.props}
                        action={this.state.action}
                        modalVisibility={this.state.urlModalVisibility}
                        onCancel={this.closeUrlModal}
                    />
                    <MemberInfoModal
                        member={this.state.member}
                        onCancel={this.closeMemberInfoModal}
                        key={this.state.randomKey}
                        {...this.props}
                        modalVisibility={this.state.memberInfoModalVisibility}/>
                </Col>
            </Row>
        </Fragment>
    }

    render() {
        return (
            <Row>
                <Col>
                    <Row>
                        <Col>{this.renderFilterView()}</Col>
                    </Row>
                    <Row>
                        <Col>{this.renderList()}</Col>
                    </Row>
                </Col>
            </Row>
        );
    }
}

export default ActivityList;
