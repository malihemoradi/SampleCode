import React, {Component} from "react";
import {Row, Col, Modal, Button} from "antd";

class MemberInfoModal extends Component {

    state = {
        member: {}
    };

    onCancel = () => {
        this.props.onCancel();
    };

    onCancel = () => this.props.onCancel();

    renderView() {
        if (!this.props.member) return;


        const member = this.props.member;
        return (<Row>
            <span>Name: &nbsp; <strong>{member.fullName}</strong></span>
            <br/><br/>
            <span>Mobile Number:  &nbsp; <strong>{member.phoneNumber}</strong></span>
            <br/><br/>
            <span>National Code: &nbsp; <strong>{member.nationalCode}</strong></span>
            <br/><br/>
            <span>Email: &nbsp; <strong>{member.email}</strong></span>
            <br/><br/>
            <span>Register Date: &nbsp; <strong>{member.createdAt}</strong></span>
            <br/><br/>
            <span>Device Info: &nbsp; os &nbsp; <strong>{member.deviceInfo.osVersion}</strong>
                &nbsp; version &nbsp; <strong>{member.deviceInfo.andoidVersion}</strong>
                &nbsp; model &nbsp; <strong>{member.deviceInfo.modelNumber}</strong>
            </span>
        </Row>);
    }

    render() {
        return (
            <Modal
                visible={this.props.modalVisibility}
                title="Member Info"
                onCancel={this.onCancel}
                footer={[
                    <Button type="danger" key="cancel" onClick={this.onCancel}>
                        Close
                    </Button>
                ]}
            >
                <Row>
                    <Col>{this.renderView()}</Col>
                </Row>
            </Modal>
        );
    }
}

export default MemberInfoModal;
