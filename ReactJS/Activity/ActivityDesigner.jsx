import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {Route, Switch} from "react-router";
import {ActionCreators as UndoActionCreators} from 'redux-undo'

import * as actionCreators from '../Store/Actions/RequestActivityActions'
import ActivityList from "./ActivityList";

function mapStateToProps(state) {
    return {
        requestActivities: state.requestActivities
    }
}

function mapDispachToProps(dispatch) {
    return bindActionCreators(Object.assign({}, actionCreators, UndoActionCreators), dispatch);
}

class ActivityDesigner extends Component {

    componentDidMount() {
        this.props.getActivities({"pageIndex": 1, "pageSize": 100});
    }

    render() {
        return (
            <React.Fragment>
                <Switch>
                    <Route
                        path="/app/activities"
                        exact
                        render={props => <ActivityList {...this.props} {...props} />}
                    />
                </Switch>
            </React.Fragment>
        );
    }
}

export default connect(mapStateToProps, mapDispachToProps)(ActivityDesigner)