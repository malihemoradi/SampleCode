import React, {Component} from "react";
import {Form, Input, Button, Icon, Alert, AutoComplete, Row, Col, Modal} from "antd";
import {bindActionCreators} from "redux";
import * as actionCreators from "../Store/Actions/NotificationsAction";
import {ActionCreators as UndoActionCreators} from "redux-undo";
import {connect} from "react-redux";
import CopyToClipBoard from "react-copy-to-clipboard";
import {FaRegCopy} from "react-icons/fa";

const {Item} = Form;
const {TextArea} = Input;


function mapDispachToProps(dispatch) {
    return bindActionCreators(Object.assign({}, actionCreators, UndoActionCreators), dispatch);
}

class SendNotificationToMember extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            msg: ""
        }
    }

    static getDerivedStateFromProps(props, state) {
        return {member: props.member,
        apiKey: props.application.apiKey};
    }

    send = () => {

        this.props.form.validateFields((error, values) => {
            const member = this.state.member;

            const msg = values.msg;
            const memberId = member._id;
            const appId = this.props.match.params.appId;
            const phoneNumber = member.phoneNumber;
            this.sendNotification(this.props.application.apiKey, memberId, phoneNumber, msg);

            this.props.form.resetFields();
            this.onCancel();
        });
    };

    sendNotification = (apiKey, memberId, phoneNumber, msg) => {
        this.props.sendNotification(apiKey, memberId, phoneNumber, msg);
    };

    generateNewApiKey = () => {
        const {appId} = this.props.match.params;
        this.props.generateNewApiKey(appId);
    };

    onCancel = () => {
        this.props.onCancel();
    };

    renderView() {

        const member = this.props.member;
        if (!member) return;
        const {getFieldDecorator} = this.props.form;


        return (<Form onFinish={this.onFinish}>
            <Item>
                {!this.props.application.apiKey ||
                this.props.application.apiKey.trim() === "" ? (
                    <span>
              <Alert
                  message='Error'
                  description='You must have an API to send notifications,please first create an api key'
                  type='error'
                  showIcon
              />
              <Button
                  type='primary'
                  size='small'
                  onClick={this.generateNewApiKey}
              >
                {" "}
                  <Icon type='thunderbolt' style={{color: "#ffb86c"}}/>
                Generate an API Key
              </Button>
            </span>
                ) : (
                    getFieldDecorator("apiKey", {
                        initialValue: this.props.application.apiKey || ""
                    })(
                        <Input
                            type='text'
                            disabled={true}
                            addonBefore='API Key'
                            addonAfter={
                                <CopyToClipBoard
                                    onCopy={this.onCopy}
                                    text={this.props.application.apiKey}
                                >
                                    <a href='' onClick={e => e.preventDefault()}>
                                        <FaRegCopy size={24}/>
                                    </a>
                                </CopyToClipBoard>
                            }
                        />
                    )
                )}
            </Item>
            <Item style={{marginBottom: "0px"}}>
                <Row>
                    <Col span={4}>
                        <span className='input-group-n'>Member Id:</span>
                    </Col>
                    <Col span={10}>
                        <span>{member._id}</span>
                    </Col>
                </Row>
            </Item>
            <Item label="message" name="msg">
                {getFieldDecorator("msg")(<TextArea type='text'/>)}
            </Item>
            <Item>
                <Button type='primary' onClick={this.send}>
                    <Icon type='message' style={{color: "#48BFD8"}}/>
                    Send
                </Button>
            </Item>
        </Form>);
    }

    render() {
        return (
            <Modal
                visible={this.props.modalVisibility}
                onCancel={this.onCancel}
                title="Send Notification"
                width={800}
                footer={[
                    <Button type="danger" key="cancel" onClick={this.onCancel}>
                        Close
                    </Button>
                ]}
            >
                <Row>
                    <Col>{this.renderView()}</Col>
                </Row>

            </Modal>
        );
    }
}

const SendNotification = Form.create()(SendNotificationToMember);
export default connect(mapDispachToProps)(SendNotification)
