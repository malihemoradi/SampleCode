import React, {Fragment} from "react";
import {Button, Card, Col, Input, Row, Table, Tooltip} from "antd";
import DeviceInfoDialog from "./DeviceInfoDialog";
import axios from "axios";
import {MEMBERS_GET_ALL, MEMBERS_COUNT_GET, App_INFO, MEMBER_LEAVE, MEMBER_DELETE} from "../Store/API/Address";
import axiosConfig from "../Store/API/AxiosConfig";
import {failedLoadingBar, successLoadingBar} from "../Store/Actions/loadingBarActions";
import {DatePicker} from 'antd';
import {FaRegTrashAlt, FaAppStoreIos, FaUser} from "react-icons/fa";
import AppsModal from "./AppsModal";
import DeleteConfirmModal from "./DeleteConfirmModal";
import ActivateConfirmModal from "./ActivateConfirmModal";
import jalaliMoment from "jalali-moment";

const {RangePicker} = DatePicker;

class AllMemberList extends React.Component {

    state = {
        prevMembers: {},
        members: {}
    };

    constructor(props) {
        super(props);
        this.showDeleteConfirmModal = this.showDeleteConfirmModal.bind(this);
        this.closeDeleteConfirmModal = this.closeDeleteConfirmModal.bind(this);
        this.deleteMember = this.deleteMember.bind(this);
        this.state = {
            infoModalVisibility: false,
            appsModalVisibility: false,
            deleteConfirmModalVisibility: false,
            randomKey: Math.round(Math.random() * 1000).toString(),
            memberId: "",
            phoneNumber: "",
            nationalCode: "",
            fullName: "",
            createdAt: "",
            lastVisit: "",
            pagination: {
                current: 1,
                pageSize: 10,
            },
            loading: false,
            membersCount: ""
        };

        this.getMembersCount();
    }

    static getDerivedStateFromProps(props, state) {
        if (props.members !== state.prevMembers) {
            return {
                members: props.members,
                prevMembers: props.members
            };
        }
        return null;
    }

    columns = [
        {
            title: 'Member Id',
            dataIndex: '_id',
            key: '_id',
        },
        {
            title: 'Full Name',
            dataIndex: 'fullName',
            key: 'fullName',
        },
        {
            title: 'National Code',
            dataIndex: 'nationalCode',
            key: 'nationalCode',
        },
        {
            title: 'Phone Number',
            dataIndex: 'phoneNumber',
            key: 'phoneNumber',
        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
        },
        {
            title: 'OTP Code',
            dataIndex: 'otp',
            key: 'otp',
        },
        {
            title: 'Verified',
            dataIndex: 'isMobileVerified',
            key: 'isMobileVerified',
            render: (text, record) => (
                <span>{text.toString()}</span>
            )
        },
        {
            title: 'Device Info',
            dataIndex: 'deviceInfo',
            key: 'deviceInfo',
            render: (text, record) => (
                <Button type="primary" onClick={() => this.showDeviceInfoModal(text)}>
                    {record.deviceInfo.osVersion}
                </Button>
            )
        },
        {
            title: 'Apps',
            dataIndex: 'apps',
            key: 'apps',
            render: (text, record) => (
                <Button
                    shape="circle"
                    title="Apps"
                    type={"primary"}
                    onClick={e => this.showAppsModal(record)}>
                    <FaAppStoreIos color="#66BB6A"/>
                </Button>
            )
        },
        {
            title: 'Created At',
            dataIndex: 'createdAt',
            key: 'createdAt',
            render: (text, record) => (
                <Tooltip title={
                    jalaliMoment(text).locale('fa').format('YYYY/MM/DD')
                }><span>{text}</span></Tooltip>
            )
        },
        {
            title: 'Last Visit',
            dataIndex: 'lastVisit',
            key: 'lastVisit',
            render: (text, record) => (
                <Tooltip title={
                    jalaliMoment(text).locale('fa').format('YYYY/MM/DD')
                }><span>{text}</span></Tooltip>
            )
        },
        {
            title: 'Active',
            dataIndex: 'isActive',
            key: 'isActive',
            render: (text, record) => (
                <Button
                    shape="circle"
                    title={record.isActive ? "Is Active" : "Is not Active"}
                    type={"primary"}
                    onClick={e => this.showActiveConfirmModal(record)}>
                    <FaUser color={record.isActive ? "#66BB6A" : "#878686"}/>
                </Button>
            )
        },
        {
            title: 'Delete',
            dataIndex: 'delete',
            key: 'delete',
            render: (text, record) => (
                <Button
                    shape="circle"
                    title="Delete"
                    type={"primary"}
                    onClick={e => this.showDeleteConfirmModal(record)}>
                    < FaRegTrashAlt color="#FF6393"/>
                </Button>
            )
        }
    ];

    componentWillReceiveProps(nextProps) {
        this.setState({members: nextProps.members});
    }

    showDeviceInfoModal = (record) => {
        this.setState({
            infoModalVisibility: true,
            selectedRecord: record
        });
    };

    closeDeviceInfoModal = () => {
        this.setState({infoModalVisibility: false});
    };

    showActiveConfirmModal = (record) => {
        this.setState({
            activeConfirmModalVisibility: true,
            member: record
        });
    };

    closeActiveConfirmModal = () => {
        this.setState({activeConfirmModalVisibility: false});
    };

    showDeleteConfirmModal = (record) => {
        this.setState({
            deleteConfirmModalVisibility: true,
            member: record
        });
    };

    closeDeleteConfirmModal = () => {
        this.setState({deleteConfirmModalVisibility: false});
    };

    showAppsModal = async (record) => {
        const appIdList = record.apps;

        const results: object[] = await Promise.all(appIdList.map(async (appId): Promise<object> => {
                const response = await axios.post(App_INFO, {appId: appId}, axiosConfig());
                return response.data.res;
            }
        ));

        this.setState({
            appsModalVisibility: true,
            appsList: results
        });
    };

    closeAppsModal = () => {
        this.setState({appsModalVisibility: false});
    };

    renderList() {
        return <Fragment>
            <Row key="listlist">
                <Col style={{margin: "20px"}}>
                    <Table
                        dataSource={this.state.members}
                        columns={this.columns}
                        showHeader={true}
                        rowKey={record => record._id.toString()}
                        locale={{emptyText: "No Route"}}
                        loading={this.state.loading}
                        pagination={{
                            ...this.state.pagination,
                            showQuickJumper: true,
                            total: 1000,
                            onChange: (page, size) => {
                                this.search({
                                    loading: true,
                                    pagination: {
                                        current: page,
                                        pageSize: size
                                    }
                                });
                            }
                        }}
                    />
                </Col>
            </Row>
        </Fragment>
    }

    renderFilterView() {
        return (
            <Card bodyStyle={{padding: "10px"}}>
                <Row>
                    <Col span={5} style={{margin: "5px"}}>
                        <Input
                            type="text"
                            addonBefore="Member Id: "
                            style={{color: "#555"}}
                            onChange={this.onMemberIdChange}
                        />
                    </Col>
                    <Col span={4} style={{margin: "5px"}}>
                        <Input
                            type="text"
                            addonBefore="Mobile: "
                            onChange={this.onMobileNumberChange}
                        />
                    </Col>
                    <Col span={4} style={{margin: "5px"}}>
                        <Input
                            type="text"
                            addonBefore="National Code: "
                            onChange={this.onNationalCodeChange}
                        />
                    </Col>
                    <Col span={4} style={{margin: "5px"}}>
                        <Input
                            type="text"
                            addonBefore="Full Name: "
                            onChange={this.onFullNameChange}
                        />
                    </Col>
                    <Col span={5} style={{margin: "5px"}}>
                        <div style={{
                            background: "#212121",
                            borderRadius: "4px"
                        }}>
                            <span style={{margin: "5px"}}>Created At: </span>
                            <RangePicker
                                onChange={this.onCreatedAtChange}/>
                        </div>
                    </Col>
                    <Col span={5} style={{margin: "5px"}}>
                        <div style={{
                            background: "#212121",
                            borderRadius: "4px"
                        }}>
                            <span style={{margin: "5px"}}>Last Visit: </span>
                            <DatePicker
                                onChange={this.onLastVisitChange}/>
                        </div>
                    </Col>
                    <Col span={2} style={{margin: "5px"}}>
                        <Button
                            type="primary"
                            onClick={this.handleSearchButton}>Search</Button>
                    </Col>
                    <Col span={4} style={{margin: "5px", padding: "6px"}}>
                        <div><span>Count:</span><span> &nbsp;  {this.state.membersCount} </span></div>
                    </Col>
                </Row>
            </Card>
        );
    }


    handleSearchButton = () => {

        if (this.state.memberId.length === 0
            && this.state.phoneNumber.length === 0
            && this.state.nationalCode.length === 0
            && this.state.fullName.length === 0
            && this.state.createdAt.length === 0
            && this.state.lastVisit.length === 0
        ) {
            return;
        }

        this.search({
            pagination: {
                current: 1,
                pageSize: 10
            }
        });
    };
    search = (params = {}) => {
        const {memberId, phoneNumber, nationalCode, fullName, createdAt, lastVisit} = this.state;
        const {current, pageSize} = (params.pagination) ? params.pagination : this.state.pagination;

        this.setState({
            loading: true,
            pagination: {
                current: current,
                pageSize: pageSize
            }
        });

        console.log(`current:  ${current} /pageSize:  ${pageSize}`);

        const query = {
            "memberId": memberId,
            "phoneNumber": phoneNumber,
            "nationalCode": nationalCode,
            "fullName": fullName,
            "createdAt": createdAt,
            "lastVisit": lastVisit,
            "pageIndex": current,
            "pageSize": pageSize
        };
        this.getMembersCount(query);
        this.getMembers(query);
    };

    getMembersCount = query => {
        axios.post(MEMBERS_COUNT_GET, query, axiosConfig())
            .then(result => {
                if (result.data.ok === 1) {
                    successLoadingBar();
                    this.setState({
                        membersCount: result.data.res.membersCount
                    });

                } else if (result.data.ok === 0) {
                    failedLoadingBar();
                    console.log(result.data.err);
                }
            })
            .catch(err => {
                failedLoadingBar();
                console.log(err);
            });
    };

    getMembers = query => {
        this.setState({loading: true});

        axios.post(MEMBERS_GET_ALL, query, axiosConfig())
            .then(result => {
                if (result.data.ok === 1) {
                    successLoadingBar();
                    this.setState({
                        loading: false,
                        members: result.data.res
                    });

                } else if (result.data.ok === 0) {
                    failedLoadingBar();
                    console.log(result.data.err);
                }
            })
            .catch(err => {
                failedLoadingBar();
                console.log(err);
            });
    };

    deleteMember = (record) => {
        const member = record;
        console.log("delete member");
        console.log(`appId: ${this.state.appId}  memberId: ${member._id}`);

        this.props.deleteMember({
            memberId: member._id
        }, member);

        //close modal
        this.closeDeleteConfirmModal();

        this.getMembersCount();
    };

    activateMember = (record) => {
        const member = record;

        let active;
        if (member.isActive) {
            active = false;
        } else {
            active = true;
        }

        this.props.activateMember({memberId: member._id, active: active}, member);

        this.closeActiveConfirmModal()
    };

    onMemberIdChange = (e) => {
        const {value} = e.target;
        this.setState({memberId: value.trim().toLowerCase()});
    };

    onMobileNumberChange = (e) => {
        const {value} = e.target;
        this.setState({phoneNumber: value});
    };

    onNationalCodeChange = (e) => {
        const {value} = e.target;
        this.setState({nationalCode: value});
    };

    onFullNameChange = (e) => {
        const {value} = e.target;
        this.setState({fullName: value});
    };

    onCreatedAtChange = (date, dateString) => {
        this.setState({createdAt: dateString});
    };

    onLastVisitChange = (date, dateString) => {
        this.setState({lastVisit: dateString});
    };

    render() {
        return (
            <Row>
                <Col>
                    <Row key="filter">
                        <Col>{this.renderFilterView()}</Col>
                    </Row>
                    <Row key="list">
                        <Col>{this.renderList()}</Col>
                        <DeviceInfoDialog
                            key={this.state.randomKey}
                            {...this.props}
                            deviceInfo={this.state.selectedRecord}
                            modalVisibility={this.state.infoModalVisibility}
                            onCancel={this.closeDeviceInfoModal}
                        />
                        <AppsModal
                            key={this.state.randomKey}
                            {...this.props}
                            appsList={this.state.appsList}
                            modalVisibility={this.state.appsModalVisibility}
                            onCancel={this.closeAppsModal}
                        />
                        <DeleteConfirmModal
                            key={this.state.randomKey}
                            {...this.props}
                            member={this.state.member}
                            modalVisibility={this.state.deleteConfirmModalVisibility}
                            onOk={this.deleteMember}
                            onCancel={this.closeDeleteConfirmModal}
                        />
                        <ActivateConfirmModal
                            key={this.state.randomKey}
                            {...this.props}
                            member={this.state.member}
                            modalVisibility={this.state.activeConfirmModalVisibility}
                            onOk={this.activateMember}
                            onCancel={this.closeActiveConfirmModal}
                        />
                    </Row>
                </Col>
            </Row>
        );
    }
}

export default AllMemberList;
