import React from "react";
import {Button, Modal, Row, Col} from "antd";

class DeviceInfoDialog extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            deviceInfo: props.deviceInfo
        }
    }


    onCancel = () => {
        this.props.onCancel();
    };

    static getDerivedStateFromProps(props, state) {
        return {deviceInfo: props.deviceInfo};
    }

    renderView() {

        if (!this.state.deviceInfo) return;

        return (<div>
            <span>OS: &nbsp; <strong>{this.state.deviceInfo.osVersion}</strong></span>
            <br/><br/>
            <span>OS Version:  &nbsp; <strong>{this.state.deviceInfo.andoidVersion}</strong></span>
            <br/><br/>
            <span>Model Number: &nbsp; <strong>{this.state.deviceInfo.modelNumber}</strong></span>
            <br/><br/>
            <span>Resolution: &nbsp; <strong>{this.state.deviceInfo.resolution}</strong></span>
        </div>);
    }

    render() {
        return (
            <Modal
                visible={this.props.modalVisibility}
                onCancel={this.onCancel}
                title="Device Info"
                footer={[
                    <Button type="danger" key="cancel" onClick={this.onCancel}>
                        Cancel
                    </Button>
                ]}
            >
                <Row>
                    <Col>{this.renderView()}</Col>
                </Row>
            </Modal>
        );
    }
}

// const InfoForm = Form.create()(DeviceInfoDialog);
export default DeviceInfoDialog;