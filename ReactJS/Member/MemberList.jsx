import React, {Fragment} from "react";
import {Button, Card, Col, DatePicker, Input, Row, Select, Table, Tooltip} from "antd";
import DeviceInfoDialog from "./DeviceInfoDialog";
import axios from "axios";
import {IoMdSend} from "react-icons/io";
import {MEMBERS_COUNT_BY_APPID_GET, MEMBERS_GET_BY_APPID} from "../Store/API/Address";
import {FaRegTrashAlt, FaUser} from "react-icons/fa";
import axiosConfig from "../Store/API/AxiosConfig";
import {failedLoadingBar, successLoadingBar} from "../Store/Actions/loadingBarActions";
import SendNotification from "./SendNotification";
import * as actionCreators from '../Store/Actions/MemberActions';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {ActionCreators as UndoActionCreators} from "redux-undo";
import DeleteConfirmModal from "./DeleteConfirmModal";
import ActivateConfirmModal from "./ActivateConfirmModal";
import jalaliMoment from "jalali-moment";

const {RangePicker} = DatePicker;


function mapStateToProps(state) {
    // let members = (state.member && state.member.length > 0) ? state.members.push(state.member) : state.members;

    return {
        members: state.members,
        loading: false
    }
}

function mapDispachToProps(dispatch) {
    return bindActionCreators(Object.assign({}, actionCreators, UndoActionCreators), dispatch);
}

class MemberList extends React.Component {

    state = {
        members: {}
    };

    constructor(props) {
        super(props);
        this.showCreateNewAppModal = this.showCreateNewAppModal.bind(this);
        this.closeCreateNewAppModal = this.closeCreateNewAppModal.bind(this);
        this.showSendNotificationModal = this.showSendNotificationModal.bind(this);
        this.closeSendNotificationModal = this.closeSendNotificationModal.bind(this);
        this.showDeleteConfirmModal = this.showDeleteConfirmModal.bind(this);
        this.closeDeleteConfirmModal = this.closeDeleteConfirmModal.bind(this);
        this.deleteMember = this.deleteMember.bind(this);
        this.state = {
            appId: props.match.params.appId,
            versionId: props.match.params.versionId,
            infoModalVisibility: false,
            sendNotificationModalVisibility: false,
            activeConfirmModalVisibility: false,
            deleteConfirmModalVisibility: false,
            randomKey: Math.round(Math.random() * 1000).toString(),
            memberId: "",
            phoneNumber: "",
            nationalCode: "",
            fullName: "",
            createdAt: "",
            lastVisit: "",
            pagination: {
                current: 1,
                pageSize: 10
            },
            loading: false,
            membersCount: ""
        };

        this.getMembersCountByAppId({appId: props.match.params.appId});
    }

    static getDerivedStateFromProps(props, state) {
        return {loading: props.loading};
    }

    columns = [
        {
            title: 'Member Id',
            dataIndex: '_id',
            key: '_id',
        },
        {
            title: 'Full Name',
            dataIndex: 'fullName',
            key: 'fullName',
        },
        {
            title: 'National Code',
            dataIndex: 'nationalCode',
            key: 'nationalCode',
        },
        {
            title: 'Phone Number',
            dataIndex: 'phoneNumber',
            key: 'phoneNumber',
        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
        },
        {
            title: 'Verification Code',
            dataIndex: 'otp',
            key: 'otp',
        },
        {
            title: 'Verified',
            dataIndex: 'isMobileVerified',
            key: 'isMobileVerified',
            render: (text, record) => (
                <span>{text}</span>
            )
        },
        {
            title: 'Device Info',
            dataIndex: 'deviceInfo',
            key: 'deviceInfo',
            render: (text, record) => {
                try {
                    return (<Button type="primary" onClick={() => this.showCreateNewAppModal(text)}>
                        {record.deviceInfo.osVersion}
                    </Button>)
                } catch (e) {
                    console.log(e)
                }
            }
        },
        {
            title: 'Notify',
            dataIndex: 'message',
            key: 'message',
            render: (text, record) => (
                <Button
                    shape="circle"
                    title="Send Message"
                    type={"primary"}
                    onClick={() => this.showSendNotificationModal(record)}>
                    <IoMdSend size={20} color="#66BB6A"/>
                </Button>
            )
        },
        {
            title: 'Last Visit',
            dataIndex: 'lastVisit',
            key: 'lastVisit',
            render: (text, record) => (
                <Tooltip title={
                    jalaliMoment(text).locale('fa').format('YYYY/MM/DD')
                }><span>{text}</span></Tooltip>
            )
        },
        {
            title: 'Created At',
            dataIndex: 'createdAt',
            key: 'createdAt',
            render: (text, record) => (
                <Tooltip title={
                    jalaliMoment(text).locale('fa').format('YYYY/MM/DD')
                }><span>{text}</span></Tooltip>
            )
        },
        {
            title: 'Active',
            dataIndex: 'isActive',
            key: 'isActive',
            render: (text, record) => (
                <Button
                    shape="circle"
                    title={record.isActive ? "Is Active" : "Is not Active"}
                    type={"primary"}
                    onClick={e => this.showActiveConfirmModal(record)}>
                    < FaUser color={record.isActive ? "#66BB6A" : "#878686"}/>
                </Button>
            )
        },
        {
            title: 'Delete',
            dataIndex: 'delete',
            key: 'delete',
            render: (text, record) => (
                <Button
                    shape="circle"
                    title="Delete"
                    type={"primary"}
                    onClick={e => this.showDeleteConfirmModal(record)}>
                    < FaRegTrashAlt color="#FF6393"/>
                </Button>
            )
        }];

    showCreateNewAppModal = (text) => {
        this.setState({
            infoModalVisibility: true,
            selectedRecord: text
        });
    };

    closeCreateNewAppModal() {
        this.setState({infoModalVisibility: false});
    };

    showSendNotificationModal(record) {
        this.setState({
            sendNotificationModalVisibility: true,
            member: record
        });
    };

    closeSendNotificationModal() {
        this.setState({sendNotificationModalVisibility: false});
    };

    showActiveConfirmModal = (record) => {
        this.setState({
            activeConfirmModalVisibility: true,
            member: record
        });
    };

    closeActiveConfirmModal = () => {
        this.setState({activeConfirmModalVisibility: false});
    };

    showDeleteConfirmModal = (record) => {
        this.setState({
            deleteConfirmModalVisibility: true,
            member: record
        });
    };

    closeDeleteConfirmModal = () => {
        this.setState({deleteConfirmModalVisibility: false});
    };

    renderLogList() {
        return <Fragment>
            <Row>
                <Col style={{margin: "20px"}}>
                    <Table
                        dataSource={this.props.members}
                        columns={this.columns}
                        showHeader={true}
                        rowKey={record => record._id}
                        locale={{emptyText: "No Route"}}
                        loading={this.state.loading}
                        pagination={{
                            ...this.state.pagination,
                            showQuickJumper: true,
                            total: 500,
                            onChange: (page, size) => {
                                this.search({
                                    loading: true,
                                    pagination: {
                                        current: page,
                                        pageSize: size
                                    }
                                });
                            }
                        }}
                    />
                </Col>
            </Row>
        </Fragment>
    }

    renderFilterView() {
        const children = [];
        // for (let i = 10; i < 36; i++) {
        //     children.push(<Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>);
        // }
        return (
            <Card bodyStyle={{padding: "10px"}}>
                <Row>
                    <Col>
                        <Row>
                            <Col span={5} style={{margin: "5px"}}>
                                <Input
                                    type="text"
                                    addonBefore="Member Id: "
                                    style={{color: "#555"}}
                                    onChange={this.onMemberIdChange}
                                />
                            </Col>
                            <Col span={4} style={{margin: "5px"}}>
                                <Input
                                    type="text"
                                    addonBefore="Mobile: "
                                    onChange={this.onMobileNumberChange}
                                />
                            </Col>
                            <Col span={4} style={{margin: "5px"}}>
                                <Input
                                    type="text"
                                    addonBefore="National Code: "
                                    onChange={this.onNationalCodeChange}
                                />
                            </Col>
                            <Col span={4} style={{margin: "5px"}}>
                                <Input
                                    type="text"
                                    addonBefore="Full Name: "
                                    onChange={this.onFullNameChange}
                                />
                            </Col>
                            <Col span={5} style={{margin: "5px"}}>
                                <div style={{
                                    background: "#212121",
                                    borderRadius: "4px"
                                }}>
                                    <span style={{margin: "5px"}}>Created At: </span>
                                    <RangePicker
                                        onChange={this.onCreatedAtChange}/>
                                </div>
                            </Col>
                            <Col span={5} style={{margin: "5px"}}>
                                <div style={{
                                    background: "#212121",
                                    borderRadius: "4px"
                                }}>
                                    <span style={{margin: "5px"}}>Last Visit: </span>
                                    <DatePicker
                                        onChange={this.onLastVisitChange}/>
                                </div>
                            </Col>
                            <Col span={2} style={{margin: "5px"}}>
                                <Button
                                    type="primary"
                                    onClick={this.handleSearchButton}>Search</Button>
                            </Col>
                            <Col span={2} style={{margin: "5px", padding: "6px"}}>
                                <div><span>Count:</span><span> &nbsp;  {this.state.membersCount} </span></div>
                            </Col>
                            <Col span={12} style={{margin: "5px"}}>
                                <Row>
                                    <Col span={4} style={{paddingTop: "5px"}}>
                                        <span color="#fff">Join with mobile:</span>
                                    </Col>
                                    <Col span={10} style={{padding: "2px"}}>
                                        <Select
                                            mode="tags"
                                            style={{width: '100%'}}
                                            placeholder="Add mobile numbers"
                                            onChange={this.onMobileForJoinChange}>
                                            {children}
                                        </Select>
                                    </Col>
                                    <Col span={2} style={{padding: "2px"}}>
                                        <Button
                                            type="primary"
                                            onClick={e => this.joinToApp()}>Join</Button>
                                    </Col>
                                </Row>
                                <Row>
                                    <span>Enter mobile numbers without comma (,)</span>
                                </Row>
                            </Col>
                        </Row>
                    </Col>

                </Row>
            </Card>
        );
    }

    handleSearchButton = () => {

        if (this.state.memberId.length === 0
            && this.state.phoneNumber.length === 0
            && this.state.nationalCode.length === 0
            && this.state.fullName.length === 0
            && this.state.createdAt.length === 0
            && this.state.lastVisit.length === 0
        ) {
            return;
        }


        this.search({
            pagination: {
                current: 1,
                pageSize: 10
            }
        });
    };

    search(params = {}) {
        const {appId} = this.state;
        const {memberId, phoneNumber, nationalCode, fullName, createdAt, lastVisit} = this.state;
        const {current, pageSize} = params.pagination;

        this.setState({
            loading: true,
            pagination: {
                current: current,
                pageSize: pageSize
            }
        });

        console.log(`current:  ${current} /pageSize:  ${pageSize}`);

        const query = {
            "appId": appId,
            "memberId": memberId,
            "phoneNumber": phoneNumber,
            "nationalCode": nationalCode,
            "fullName": fullName,
            "createdAt": createdAt,
            "lastVisit": lastVisit,
            "pageIndex": current,
            "pageSize": pageSize
        };
        this.props.getMembersByAppId(query);
        this.getMembersCountByAppId(query);
    };

    joinToApp = () => {
        const {appId, mobilesForJoin} = this.state;
        const query = {
            "appId": appId,
            "phoneNumbers": mobilesForJoin,
        };
        this.props.joinMemberToAppWithMobile(query);
    };

    deleteMember = (record) => {
        const member = record;

        console.log("delete member");
        console.log(`appId: ${this.state.appId}  memberId: ${member._id}`);

        this.props.leaveMemberFromApp({
            appId: this.state.appId,
            memberId: member._id
        }, member);

        //close modal
        this.closeDeleteConfirmModal();

        this.getMembersCountByAppId({appId: this.props.match.params.appId});
    };

    // activateMember = (record) => {
    //     const member = record;
    //
    //     this.props.activateMember({memberId: member._id, activate: true}, member)
    // };

    getMembers = query => {
        axios.post(MEMBERS_GET_BY_APPID, query, axiosConfig())
            .then(result => {
                if (result.data.ok === 1) {
                    successLoadingBar();
                    this.setState({
                        loading: false,
                        members: result.data.res
                    });
                } else if (result.data.ok === 0) {
                    failedLoadingBar();
                    console.log(result.data.err);
                }
            })
            .catch(err => {
                failedLoadingBar();
                console.log(err);
            });
    };

    getMembersCountByAppId = query => {
        axios.post(MEMBERS_COUNT_BY_APPID_GET, query, axiosConfig())
            .then(result => {
                if (result.data.ok === 1) {
                    successLoadingBar();
                    this.setState({
                        membersCount: result.data.res.membersCount
                    });

                } else if (result.data.ok === 0) {
                    failedLoadingBar();
                    console.log(result.data.err);
                }
            })
            .catch(err => {
                failedLoadingBar();
                console.log(err);
            });
    };

    activateMember = (record) => {
        const member = record;

        let active;
        if (member.isActive) {
            active = false;
        } else {
            active = true;
        }

        this.props.activateMember({memberId: member._id, active: active}, member);

        this.closeActiveConfirmModal()
    };

    onMemberIdChange = (e) => {
        const {value} = e.target;
        this.setState({memberId: value.trim().toLowerCase()});
    };

    onMobileNumberChange = (e) => {
        const {value} = e.target;
        this.setState({phoneNumber: value});
    };

    onNationalCodeChange = (e) => {
        const {value} = e.target;
        this.setState({nationalCode: value});
    };

    onFullNameChange = (e) => {
        const {value} = e.target;
        this.setState({fullName: value});
    };

    onCreatedAtChange = (date, dateString) => {
        this.setState({createdAt: dateString});
    };

    onLastVisitChange = (date, dateString) => {
        this.setState({lastVisit: dateString});
    };

    onMobileForJoinChange = (value) => {
        this.setState({mobilesForJoin: value});
    };

    render() {
        return (
            <Row>
                <Col>
                    <Row>
                        <Col>{this.renderFilterView()}</Col>
                    </Row>
                    <Row>
                        <Col>{this.renderLogList()}</Col>
                        <DeviceInfoDialog
                            key={this.state.randomKey}
                            {...this.props}
                            deviceInfo={this.state.selectedRecord}
                            modalVisibility={this.state.infoModalVisibility}
                            onCancel={this.closeCreateNewAppModal}
                        />
                        <SendNotification
                            key={this.state.randomKey}
                            {...this.props}
                            member={this.state.member}
                            modalVisibility={this.state.sendNotificationModalVisibility}
                            onCancel={this.closeSendNotificationModal}
                        />
                        <DeleteConfirmModal
                            key={this.state.randomKey}
                            {...this.props}
                            member={this.state.member}
                            modalVisibility={this.state.deleteConfirmModalVisibility}
                            onOk={this.deleteMember}
                            onCancel={this.closeDeleteConfirmModal}
                        />
                        <ActivateConfirmModal
                            key={this.state.randomKey}
                            {...this.props}
                            member={this.state.member}
                            modalVisibility={this.state.activeConfirmModalVisibility}
                            onOk={this.activateMember}
                            onCancel={this.closeActiveConfirmModal}
                        />
                    </Row>
                </Col>
            </Row>
        );
    }
}

export default connect(mapStateToProps, mapDispachToProps)(MemberList)
