import React from "react";
import {Button, Modal, Row, Col} from "antd";

class AppsModal extends React.Component {

    onCancel = () => {
        this.props.onCancel();
    };
    onOk = () => {
        this.props.onOk(this.props.member);
    };

    render() {
        return (
            <Modal
                visible={this.props.modalVisibility}
                onCancel={this.onCancel}
                onOk={this.onOk}
                title="Delete member"
                footer={[
                    <Button type="danger" key="cancel" onClick={this.onCancel}>
                        Cancel
                    </Button>,
                    <Button type="primary" key="accept" onClick={this.onOk}>
                        Accept
                    </Button>,
                ]}
            >
                <Row>
                    <Col>
                        <p> Are you sure you want to delete the member? </p>
                    </Col>
                </Row>
            </Modal>
        );
    }
}

export default AppsModal;