import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {Prompt, Route, Switch} from "react-router";
import {ActionCreators as UndoActionCreators} from 'redux-undo'

import * as actionCreators from '../Store/Actions/MemberActions'
import AllMemberList from "./AllMemberList";

function mapStateToProps(state) {
    return {
        members: state.members,
        serviceDesignerWatchChangesState: state.serviceDesignerWatchChangesState,
        serviceDesignerIsDirtyState: state.serviceDesignerIsDirtyState,
    }
}


function mapDispachToProps(dispatch) {
    return bindActionCreators(Object.assign({}, actionCreators, UndoActionCreators), dispatch);
}


class MemberDesigner extends Component {

    componentDidMount() {
        this.props.getAllMembers({"pageIndex": 1, "pageSize": 100});
    }

    render() {
        return (
            <React.Fragment>
                <Prompt when={this.props.serviceDesignerIsDirtyState} message="Discard Unsaved Changes?"/>
                <Switch>
                    <Route
                        path="/app/members"
                        exact
                        render={props => <AllMemberList {...this.props} {...props} />}
                    />
                </Switch>
            </React.Fragment>
        );
    }
}

export default connect(mapStateToProps, mapDispachToProps)(MemberDesigner)