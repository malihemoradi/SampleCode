import React from "react";
import {Button, Modal, Row, Col} from "antd";

class AppsModal extends React.Component {

    onCancel = () => {
        this.props.onCancel();
    };

    renderView() {

        if (!this.props.modalVisibility) return;

        const appsView = this.props.appsList.map((appInfo, i) => {
            console.log(appInfo);
            if (!appInfo) return;
            return (
                <Row key={i.toString()}>
                    <Col span={7} style={{margin: "6px"}}>
                        {appInfo._id}
                    </Col>
                    <Col span={6} style={{margin: "6px"}}>
                        {appInfo.name}
                    </Col>
                    <Col span={5} style={{margin: "6px"}}>
                        {appInfo.address}
                    </Col>
                    <Col span={3} style={{margin: "6px"}}>
                        {(appInfo.access === 1) ? "Public" : "Private"}
                    </Col>
                </Row>);
        });

        return appsView;
    }

    render() {
        return (
            <Modal
                visible={this.props.modalVisibility}
                onCancel={this.onCancel}
                title="Member Applications"
                width={700}
                footer={[
                    <Button type="danger" key="cancel" onClick={this.onCancel}>
                        Cancel
                    </Button>
                ]}
            >
                <Row>

                    <Col>
                        <Row key="title">
                            <Col span={7} style={{margin: "6px"}}>
                                <strong>Id</strong>
                            </Col>
                            <Col span={6} style={{margin: "6px"}}>
                                <strong>Name</strong>
                            </Col>
                            <Col span={5} style={{margin: "6px"}}>
                                <strong>Address</strong>
                            </Col>
                            <Col span={3} style={{margin: "6px"}}>
                                <strong>Access</strong>
                            </Col>
                        </Row>
                        <hr size={1} style={{margin: "0px"}}/>
                        {this.renderView()}
                    </Col>
                </Row>
            </Modal>
        );
    }
}

AppsModal.defaultProps = {
    appsList: []
};

export default AppsModal;