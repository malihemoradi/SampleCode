import React from "react";
import {Button, Modal, Row, Col} from "antd";

class ActivateConfirmModal extends React.Component {

    onCancel = () => {
        this.props.onCancel();
    };
    onOk = () => {
        this.props.onOk(this.props.member);
    };

    render() {
        return (
            <Modal
                visible={this.props.modalVisibility}
                onCancel={this.onCancel}
                onOk={this.onOk}
                title="Activate member"
                footer={[
                    <Button type="danger" key="cancel" onClick={this.onCancel}>
                        No
                    </Button>,
                    <Button type="primary" key="accept" onClick={ this.onOk}>
                        Yes
                    </Button>,
                ]}
            >
                <Row>
                    <Col>
                        <p> Are you sure you want to {((this.props.member) &&(this.props.member.isActive))? "disable":"activate"} the member? </p>
                    </Col>
                </Row>
            </Modal>
        );
    }
}

export default ActivateConfirmModal;