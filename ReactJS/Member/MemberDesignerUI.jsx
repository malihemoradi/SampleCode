import React, {Component} from "react";
import {Route, Switch, Prompt} from "react-router";
import {bindActionCreators} from "redux";
import {connect} from 'react-redux'
import {ActionCreators as UndoActionCreators} from 'redux-undo'

import * as actionCreators from '../Store/Actions/MemberActions'
import MemberList from './MemberList'

function mapStateToProps(state) {
    return {
        members: state.members,
        serviceDesignerWatchChangesState: state.serviceDesignerWatchChangesState,
        serviceDesignerIsDirtyState: state.serviceDesignerIsDirtyState,
    }
}


function mapDispachToProps(dispatch) {
    return bindActionCreators(Object.assign({}, actionCreators, UndoActionCreators), dispatch);
}

class MemberDesignerUI extends Component {

    componentDidMount() {
        const {appId} = this.props.match.params;
        this.props.getMembersByAppId({"appId": appId, "pageIndex": 1, "pageSize": 100});
    }

    render() {
        return (
            <React.Fragment>
                <Prompt when={this.props.serviceDesignerIsDirtyState} message="Discard Unsaved Changes?"/>
                <Switch>
                    <Route
                        path="/app/:appId/versions/:versionId/members"
                        exact
                        render={props => <MemberList {...this.props} {...props} />}
                    />
                </Switch>
            </React.Fragment>
        );
    }
}

export default connect(mapStateToProps, mapDispachToProps)(MemberDesignerUI)
