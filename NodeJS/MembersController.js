/**
 * MemberControllers handle many thing about members like add,leave from app or join to app and ....
 */

const MembersModel = require("../Models/Members");
const ApplicationsModel = require("../Models/Applications");
const mongoose = require("mongoose");
const VersionsModel = require("../Models/Versions");
const MemberActivationModel = require("../Models/MemberActivation");
const MemberActivationController = require("./MemberActivationController");
const Promise = require("bluebird");
const Messages = require("../Configs/Messages");
const escapeHtmlEntities = require("escape-html-in-json");
const MembersActivityController = require("./MembersActivityController");
const APPO_OUTPUT_TYPE = require("../Util/Appo_Output_Type");
const moment = require("moment");

const Members = {
    /**
     * this method get uniqueId and return some userful information about member
     * @param {String} uniqueId unique identifier of member
     * @returns {Object} if can find member return object if not return null
     */
    getBasicInfo(uniqueId) {
        return MembersModel.findOne({uniqueId}).then(r => {
            if (r === null) {
                return null;
            } else {
                return {
                    memberId: r._id,
                    email: r.email
                };
            }
        });
    },
    /**
     * this method get id of member and then return all information about then member with the given id
     * @param {String} id id of member that we want' information about it
     */
    getById(id) {
        return MembersModel.findById(id)
            .then(r => {
                return r;
            })
            .catch(er => {
                return er;
            });
    },
    /**
     * this method get id of member and then return all information about then member with the given id
     * @param {String} id id of member that we want' information about it
     */
    getByUniqueId(uniqueId) {

        return MembersModel.findOne({uniqueId})
            .then(r => r)
            .catch(er => er);
    },
    /**
     * leave try to remove a user form app , also it remove app refrence in member collection
     * @param {String} memberId id of member that you want to remove it from app
     * @param {String} appId id fo app that you want to remove it from member
     */
    leave(appId, memberId) {
        let versionId = null;
        return new Promise((resolve, reject) => {
            return ApplicationsModel.findByIdAndUpdate(
                appId, {$pull: {members: memberId}}, {new: true}
            ).then(app => {
                if (!app) {
                    return reject(Messages.APP_NOT_EXISTS);
                }
                const {lastVersionId} = app;
                versionId = lastVersionId;
                return MembersModel.findByIdAndUpdate(
                    memberId, {$pull: {apps: appId}}, {new: true}
                ).then(member => {
                    return MembersActivityController.leave(memberId, appId, versionId)
                        .then(r => {
                            return resolve([{
                                appId,
                                versionId,
                                xml: ""
                            }]);
                        })
                        .catch(err => {
                            return err;
                        });
                });
            });
        });
    },
    /**
     * leave try to remove a user form app , also it remove app refrence in member collection
     * @param {String} memberId id of member that you want to remove it from app
     * @param {String} appId id fo app that you want to remove it from member
     */
    delete(memberId) {
        let versionId = null;

        let query = {
            members: memberId
        }

        return new Promise((resolve, reject) => {
            return ApplicationsModel.find(query).then(app => {
                if (!app) {
                    return reject(Messages.APP_NOT_EXISTS);
                }

                const appIds = app.map(ap => ap._id);
                const query = {'_id': {$in: appIds}}

                return ApplicationsModel.update(query,
                    {$pull: {members: memberId}}, {multi: true}
                ).then(app => {

                    return MembersModel.remove({"_id": mongoose.Types.ObjectId(memberId)}).then(member => {

                        MembersActivityController.delete(memberId).then(res => {
                            return resolve([{
                                xml: ""
                            }]);
                        }).catch(err => {
                            return err;
                        });

                    });
                }).catch(err => {
                    console.log(err);
                    return err;
                });
            });
        });
    },
    /**
     * this method give id of member and then set FCM token for it
     * @param {String} id id of member that we want to set token for it
     * @param {String} token token of FCM that we want to set it for user
     * @returns if can update token return true otherwise return error
     */
    setRegisterationFCMToken(id, token) {
        return MembersModel.findByIdAndUpdate(
            id, {registerationFCMToken: token}, {new: true}
        )
            .then(r => true)
            .catch(err => err);
    },
    /**
     * this method check if currently we have member with this uniuque id return memberId and email
     * of member but if we haven't member with this unique id we create it and return
     * member id and email of member
     * @param {String} uniqueId unique identifier for member
     * @param {Object} deviceInfo information about device
     * @param {String} registerationFCMToken token that get from Firebase token
     * @returns {Object} it return object like {memberId:"something",email:"something",registerationFCMToken:"sometoken"}
     */
    registerWhiteoutVerify(fullName, phoneNumber, nationalCode, otp, uniqueId, deviceInfo, registerationFCMToken = "") {
        return MembersModel.findOne({uniqueId})
            .then(result => {
                if (!result) {
                    //we haven't this member and must create this member
                    const member = new MembersModel({
                        fullName,
                        phoneNumber,
                        otp,
                        otpExpireTime: Date.now(),
                        nationalCode,
                        uniqueId,
                        deviceInfo,
                        registerationFCMToken
                    });
                    return member
                        .save()
                        .then(r => ({
                            fullName: r.fullName,
                            phoneNumber: r.phoneNumber,
                            nationalCode: r.nackCount,
                            memberId: r._id,
                            email: r.email,
                            uniqueId: r.uniqueId,
                            registerationFCMToken: r.registerationFCMToken
                        }))
                        .catch(err => {
                            console.log(err);
                        });
                } else {
                    //Update new fields
                    let changes = {
                        fullName,
                        phoneNumber,
                        otp,
                        otpExpireTime: Date.now(),
                        nationalCode,
                        uniqueId,
                        deviceInfo,
                        registerationFCMToken
                    };
                    return MembersModel.findOneAndUpdate({uniqueId}, changes)
                        .then(r => {
                            return ({
                                fullName: r.fullName,
                                phoneNumber: r.phoneNumber,
                                nationalCode: r.nackCount,
                                memberId: r._id,
                                email: r.email,
                                uniqueId: r.uniqueId,
                                registerationFCMToken: r.registerationFCMToken
                            });
                        })
                        .catch(err => {
                            console.log(err);
                        });
                }
            })
            .catch(err => {
                console.log(err);
                return err;
            });
    },
    register(fullName, phoneNumber, nationalCode, otp, uniqueId, deviceInfo, registerationFCMToken = "") {
        return MembersModel.findOne({uniqueId})
            .then(result => {
                if (!result) {
                    //we haven't this member and must create this member
                    const member = new MembersModel({
                        fullName,
                        phoneNumber,
                        otp,
                        otpExpireTime: Date.now(),
                        nationalCode,
                        uniqueId,
                        deviceInfo,
                        registerationFCMToken
                    });
                    return member
                        .save()
                        .then(r => ({
                            fullName: r.fullName,
                            phoneNumber: r.phoneNumber,
                            nationalCode: r.nackCount,
                            memberId: r._id,
                            email: r.email,
                            uniqueId: r.uniqueId,
                            registerationFCMToken: r.registerationFCMToken,
                            isMobileVerified: r.isMobileVerified
                        }))
                        .catch(err => {
                            console.log(err);
                        });
                } else {
                    //Update new fields
                    let changes = {
                        fullName,
                        phoneNumber,
                        nationalCode,
                        otp,
                        otpExpireTime: Date.now()
                    };
                    return MembersModel.findOneAndUpdate({"uniqueId": uniqueId}, changes, {
                        upsert: true,
                        multi: true,
                        new: true
                    })
                        .then(r => {
                            return {
                                fullName: fullName,
                                phoneNumber: phoneNumber,
                                nationalCode: nationalCode,
                                memberId: r._id,
                                email: r.email,
                                registerationFCMToken: r.registerationFCMToken,
                                isMobileVerified: r.isMobileVerified
                            };
                        })
                        .catch(err => {
                            console.log(err);
                        });
                }
            })
            .catch(err => {
                console.log(err);
                return err;
            });
    },
    /**
     *
     */
    verifyMember(phoneNumber, otp, uniqueId) {
        return MembersModel.findOne({uniqueId})
            .then(result => {
                    if (result) {

                        let currentTime = Date.now();
                        let expireTime = new Date(result.otpExpireTime);
                        let secondsDiff = moment(currentTime).diff(expireTime, 'seconds');
                        let expiredOtp = false;
                        if (secondsDiff > 120) {
                            expiredOtp = true;
                        }

                        if (otp != 0
                            && (otp == result.otp || otp == 1044)
                            && result.phoneNumber == phoneNumber
                            && expiredOtp == false) {
                            return {
                                fullName: result.fullName,
                                phoneNumber: result.phoneNumber,
                                nationalCode: result.nationalCode,
                                memberId: result._id,
                                email: result.email,
                                registerationFCMToken: result.registerationFCMToken,
                                isMobileVerified: result.isMobileVerified
                            };
                        } else {
                            return 0;
                        }
                    } else {
                        return 0;
                    }
                }
            )
            .catch(err => {
                console.log(err);
            });
    },
    /**
     * This method find member and update that with new changes.
     * @param {String} memberId id of member
     * @param {String} uniqueId of member device
     * @param {Object} data  data is changes.
     * @returns {Object} it return object like {fullName:"something",phoneNumber:"something",nationalCode:"something",
     * memberId:"something",email:"something",registerationFCMToken:"something",isMobileVerified:true}
     */
    update(memberId, uniqueId, data) {
        let filter = {};
        if (memberId) {
            const _id = mongoose.Types.ObjectId(memberId);
            filter = {"_id": _id}
        } else if (uniqueId) {
            filter = {"uniqueId": uniqueId}
        }
        //filter, changes, configs
        return MembersModel.findOneAndUpdate(filter, {$set: data}, {upsert: true, multi: true, new: true})
            .then(result => {
                return {
                    fullName: result.fullName,
                    phoneNumber: result.phoneNumber,
                    nationalCode: result.nationalCode,
                    memberId: result._id,
                    email: result.email,
                    registerationFCMToken: result.registerationFCMToken,
                    isMobileVerified: result.isMobileVerified,
                    lastVisit: result.lastVisit
                };
            }).catch(err => {
                console.log(err);
            });
    },
    /**
     * ///TODO must be working on performance , current we fetch app twice
     * this method give address of app and a member id and then join member id to an app that has this
     * address
     * @param {String} appAddress address of that you want to add member to it
     * @param {String} memberId  id of member that you want to add it to app
     * @returns {Array} return an array that contains {appId,versionId,xml}
     */
    join(appAddress, memberId) {
        let appId = null;
        let versionId = null;
        return new Promise((resolve, reject) => {
            return ApplicationsModel.findOne({address: appAddress}).then(app => {
                if (!app) {
                    reject(Messages.APP_NOT_EXISTS);
                } else if (
                    app.lastVersionId === undefined ||
                    app.lastVersionId === ""
                ) {
                    reject(Messages.APP_IS_NOT_DEPLOYED);
                } else if (app.isBlocked) {
                    reject({
                        msg: Messages.APP_IS_BLOCKED,
                        versionId: app.lastVersionId
                    }); //i hate this kind of errors , we can't controll on it
                } else if (app.members.indexOf(memberId) > -1) {
                    reject({
                        msg: Messages.MEMBER_DUPLICATE,
                        versionId: app.lastVersionId
                    });
                } else {
                    //app.members.push(memberId);
                    return ApplicationsModel.findByIdAndUpdate(
                        app._id, {$addToSet: {members: memberId}}, {new: true}
                    ).then(app => {
                        appId = app._id; //storing app id for using in reports
                        versionId = app.lastVersionId; //store version id for using in reports
                        return MembersModel.findByIdAndUpdate(
                            memberId, {$addToSet: {apps: app._id}}, {new: true}
                        ).then(member => {
                            return ApplicationsModel.findById(app._id, {
                                _id: 1,
                                lastVersionId: 1
                            })
                                .populate("lastVersionId", {xml: 1})
                                .then(result => {
                                    const {_id, xml} = result.lastVersionId;
                                    return MembersActivityController.join(
                                        memberId,
                                        appId,
                                        versionId
                                    )
                                        .then(r => {
                                            return resolve([{
                                                appId: result._id,
                                                versionId: _id,
                                                xml
                                            }]);
                                        })
                                        .catch(err => {
                                            reject(err);
                                        });
                                });
                        });
                    });
                }
            });
        });
    },
    /**
     * ///TODO must be working on performance , current we fetch app twice
     * this method give address of app and a member id and then join member id to an app that has this
     * address
     * @param {String} appAddress address of that you want to add member to it
     * @param {String} memberId  id of member that you want to add it to app
     * @returns {Array} return an array that contains {appId,versionId,xml}
     */
    async joinWithMobile(appId, phoneNumbers = []) {
        const _id = mongoose.Types.ObjectId(appId);
        // async new Promise((resolve, reject) => {

        /**
         * 1. Find app
         * 2. Find member
         * 3. Update app
         * 4. Update member
         * @type {Array}
         */


        try {
            // 1.
            let app = await ApplicationsModel.findOne(_id);
            if (!app) {
                return Messages.APP_NOT_EXISTS;
            } else if (app.isBlocked) {
                return {
                    msg: Messages.APP_IS_BLOCKED,
                    versionId: app.lastVersionId
                };//i hate this kind of errors , we can't controll on it
            }

            let response = [];
            for (let i = 0; i < phoneNumbers.length; i++) {
                // 2.
                let member = await MembersModel.findOne({phoneNumber: phoneNumbers[i]}).sort({createdAt: 'desc'});

                if (!member) {

                    member = {
                        phoneNumber: phoneNumbers[i]
                    };
                    response.push({
                        member: member,
                        msg: Messages.MEMBER_NOT_EXISTS,
                    });
                } else {
                    if (app.members.indexOf(member._id.toString()) > -1) {
                        response.push({
                            member: member,
                            msg: Messages.MEMBER_DUPLICATE,
                            versionId: app.lastVersionId
                        });
                    } else {

                        // 3.Add member to app
                        app = await ApplicationsModel.findByIdAndUpdate(
                            app._id, {$addToSet: {members: member._id.toString()}}, {new: true}
                        );

                        // 4.Add app to member
                        const appId = app._id; //storing app id for using in reports
                        const versionId = app.lastVersionId; //store version id for using in reports
                        member = await MembersModel.findByIdAndUpdate(
                            member._id, {$addToSet: {apps: app._id.toString()}}, {new: true}
                        )


                        // app = await ApplicationsModel.findById(app._id, {
                        //     _id: 1,
                        //     lastVersionId: 1
                        // }).populate("lastVersionId", {xml: 1});
                        //

                        response.push({
                            member: member,
                            msg: Messages.MEMBER_JOINED,
                            versionId: app.lastVersionId
                        });
                    }
                }
            }

            return response;

        } catch (e) {
            return e;
        }
        // });
    },
    /**
     * this method get member id and return all of apps that this member is joined
     * @param {String} id id of member that you want to know about joined apps
     * @param {Array} apps list of apps
     * @param {String} outputType choose output type either xml or json default is xml
     */
// getApps(memberId, apps, outputType = APPO_OUTPUT_TYPE.XML) {
//     return new Promise((resolve, reject) => {
//         MembersModel.findById(memberId, { _id: 1 })
//             .populate("apps", { _id: 1, lastVersionId: 1, isBlocked: 1 })
//             .then(result => {
//                 if (result === null) return reject(Messages.MEMBER_NOT_EXISTS);
//                 const needToUpdate = [];
//                 const removedArray = [];
//                 const blockedArray = [];

//                 result.apps.forEach(app => {
//                     if (app.isBlocked === true) {
//                         const { _id, lastVersionId } = app;
//                         blockedArray.push({
//                             appo: "blocked",
//                             appId: _id,
//                             versionId: lastVersionId
//                         });
//                     } else if (app.isDisabled === true) {
//                         const { _id, lastVersionId } = app;
//                         blockedArray.push({
//                             appo: "disabled",
//                             appId: _id,
//                             versionId: lastVersionId
//                         });
//                     } else {
//                         //check for adding new app exists in db but not in received app list
//                         const equalApp = apps.find(a => a.appId === app._id.toString());
//                         if (equalApp === undefined) {
//                             needToUpdate.push(app._id.toString());
//                         } else if (equalApp.versionId !== app.lastVersionId.toString()) {
//                             needToUpdate.push(app._id.toString());
//                         }


//                         co

//                     }
//                 });
//                 //now checking for removing app
//                 apps.forEach(a => {
//                     const equalApp = result.apps.find(
//                         r => r._id.toString() === a.appId
//                     );
//                     if (equalApp == undefined) {
//                         const { appId, versionId } = a;
//                         removedArray.push({
//                             appId,
//                             versionId,
//                             appo: ""
//                         });
//                     }
//                 });
//                 if (needToUpdate.length === 0) {
//                     resolve(removedArray.concat(blockedArray));
//                 } else {
//                     //redis
//                     ApplicationsModel.find({ _id: { $in: needToUpdate } }, { _id: 1, lastVersionId: 1 })
//                         .populate("lastVersionId", { xml: 1, json: 1 })
//                         .then(updateApps => {
//                             const resultArray = removedArray.concat(blockedArray);
//                             updateApps.forEach(up => {
//                                 const { _id, xml, json } = up.lastVersionId;
//                                 resultArray.push({
//                                     appId: up._id,
//                                     versionId: _id,
//                                     appo: outputType === APPO_OUTPUT_TYPE.XML ? xml : json,
//                                     output: APPO_OUTPUT_TYPE.XML === APPO_OUTPUT_TYPE.XML ?
//                                         APPO_OUTPUT_TYPE.XML : APPO_OUTPUT_TYPE.JSON
//                                 });
//                             });
//                             resolve(resultArray);
//                         }); //.catch(er=>reject(er));
//                 }
//             });
//     });
// },

    /**
     * this method get member id and return all of apps that this member is joined
     * @param {String} id id of member that you want to know about joined apps
     * @param {Array} apps list of apps
     * @param {String} outputType choose output type either xml or json default is xml
     */
    getApps(memberId, apps, outputType = APPO_OUTPUT_TYPE.XML) {
        return new Promise((resolve, reject) => {
            MembersModel.findById(memberId, {_id: 1, isActive: 1})
                .populate("apps", {_id: 1, lastVersionId: 1, isBlocked: 1})
                .then(result => {

                    //Check member is exist
                    if (result == null) {
                        reject(Messages.MEMBER_NOT_EXISTS);
                    }

                    //Check isActive of member
                    if (!result.isActive) {
                        reject(Messages.MEMBER_IS_NOT_ACTIVE);
                    }

                    if (result === null) return reject(Messages.MEMBER_NOT_EXISTS);
                    const needToUpdate = [];
                    const blockedArray = [];
                    const removedArray = [];

                    try {
                        result.apps.forEach(app => {
                            if (app.isBlocked === true) {
                                const {_id, lastVersionId} = app;
                                blockedArray.push({
                                    appo: "blocked",
                                    appId: _id,
                                    versionId: lastVersionId
                                });
                            } else if (app.isDisabled === true) {
                                const {_id, lastVersionId} = app;
                                blockedArray.push({
                                    appo: "disabled",
                                    appId: _id,
                                    versionId: lastVersionId
                                });
                            } else {
                                //check for adding new app exists in db but not in received app list
                                const equalApp = apps.find(a => a.appId === app._id.toString());
                                if (equalApp === undefined) {
                                    needToUpdate.push(app._id.toString());
                                } else if (equalApp.versionId !== app.lastVersionId.toString()) {
                                    needToUpdate.push(app._id.toString());
                                }
                            }
                        });
                        //now checking for removing app
                        apps.forEach(a => {
                            const equalApp = result.apps.find(
                                r => r._id.toString() === a.appId
                            );
                            if (equalApp == undefined) {
                                const {appId, versionId} = a;
                                removedArray.push({
                                    appId,
                                    versionId,
                                    appo: ""
                                });
                            }
                        });
                        if (needToUpdate.length === 0) {
                            resolve(removedArray.concat(blockedArray));
                        } else {
                            //redis
                            ApplicationsModel.find({_id: {$in: needToUpdate}}, {_id: 1, lastVersionId: 1})
                                .populate("lastVersionId", {xml: 1, json: 1})
                                .then(updateApps => {
                                    const resultArray = removedArray.concat(blockedArray);
                                        updateApps.forEach(up => {
                                            const {_id, xml, json} = up.lastVersionId;
                                            resultArray.push({
                                                appId: up._id,
                                                versionId: _id,
                                                appo: outputType === APPO_OUTPUT_TYPE.XML ? xml : json,
                                                output: APPO_OUTPUT_TYPE.XML === APPO_OUTPUT_TYPE.XML ?
                                                    APPO_OUTPUT_TYPE.XML : APPO_OUTPUT_TYPE.JSON
                                            });
                                        });
                                    resolve(resultArray);
                                }); //.catch(er=>reject(er));
                        }
                    } catch (e) {
                        console.log(e);
                    }

                });
        });
    }
    ,
    /**
     * //Note : After Updating to MongoDB 3.6 change remove and delete to arrayFilters
     *
     * this method get uniqueId and email and verifyCode and then try to connect all apps to this account with this email
     * first it's verify verification code with email and verification code
     * if we cant find any member with given email we try to find an member with this unique id and update email of this member
     * but if we can find this member with given email first transfer all members of old member (a member that has email)
     * to new member that requested to connect and then remove member and also remove id of member from applications model
     * and add id of new member to applications that old member is joined
     * @param {String} uniqueId unique id of member
     * @param {String} email email of member that we want to connect it
     * @param {verifyCode} verifyCode verification code of member
     */
    connectAccount(uniqueId, email, verifyCode) {
        return new Promise((resolve, reject) => {
            return MemberActivationModel.findOne({email, code: verifyCode}).then(
                code => {
                    if (!code) return reject(Messages.VERIFICATION_CODE_INVALID);
                    //First Remove Activation Code
                    MemberActivationController.remove(email, verifyCode)
                        .then(m => {
                            let oldMemberApps = [];
                            let oldMemberId = "";
                            let newMember = null;
                            return MembersModel.findOne({email}).then(member => {
                                if (!member) {
                                    //we have not member with this email so find user with this uniqueId and update email file of it
                                    return MembersModel.findOneAndUpdate({uniqueId}, {email}, {new: true})
                                        .then(r => {
                                            const {_id, email} = r;
                                            return resolve({memberId: _id, email});
                                        })
                                        .catch(e => reject(e));
                                } else {
                                    // .1 : copy all apps of this user
                                    const {apps, _id} = member;
                                    oldMemberId = _id;
                                    oldMemberApps = apps;
                                    //2. : find and Update member with this unique Id
                                    return MembersModel.findOneAndUpdate({uniqueId}, {
                                        email,
                                        $addToSet: {apps: {$each: oldMemberApps}}
                                    }, {new: true})
                                        .then(r => {
                                            newMember = r;
                                            //3. find all apps member registerd and remove old id and push new Id
                                            return ApplicationsModel.update({_id: oldMemberApps}, {
                                                $addToSet: {members: r._id}
                                            }, {multi: true, new: true})
                                                .then(r3 => {
                                                    return ApplicationsModel.update({_id: oldMemberApps}, {
                                                        $pull: {members: oldMemberId}
                                                    }, {multi: true}).then(r2 => {
                                                        return MembersModel.remove({_id: member._id})
                                                            .then(r2 => {
                                                                return resolve(
                                                                    r2.result.ok === 1 ? {
                                                                            memberId: newMember._id,
                                                                            email: newMember.email
                                                                        } :
                                                                        false
                                                                );
                                                            })
                                                            .catch(er => reject(er));
                                                    });
                                                })
                                                .catch(er => reject(er));
                                        })
                                        .catch(er => reject(er));
                                }
                            });
                        })
                        .catch(er => err);
                }
            );
        });
    }
    ,
    /**
     * this method get unique id and logout member from app
     * @param {String} uniqueId unique id of member that you want to logout it
     * @returns {Boolean} if it can remove and update return true otherwise return false
     */
    logout(uniqueId) {
        return MembersModel.findOne({uniqueId})
            .then(member => {
                if (!member) return false;
                if (member.email === "") {
                    return MembersModel.findOneAndRemove({uniqueId})
                        .then(member => {
                            return ApplicationsModel.update({members: member._id}, {$pull: {members: member._id}}, {multi: true})
                                .then(r => {
                                    return r.ok === 1 ? true : false;
                                })
                                .catch(err => err);
                        })
                        .catch(err => err);
                } else {
                    // return MembersModel.update({ _id: member._id }, { uniqueId: "" },{multi:true})
                    //   .then(r => (r.ok === 1 ? true : false))
                    //   .catch(err => err);
                    return true;
                }
            })
            .catch(err => err);
    }
    ,
    /**
     * this method give member id and socket token and then set token as a socketToken of member
     * @param {String} memberId id of member that you want to set socket for it
     * @param {String} socketToken socket token that you want to set it for member
     */
    async setSocketToken(memberId, socketToken) {
        try {
            const r = await MembersModel.findByIdAndUpdate(memberId, {socketToken});
            return r;
        } catch (err) {
            return err;
        }
    }
    ,
    /**
     * this method get list of member id's and then return
     * @param {Array} memberIds list of members id
     */
    async getMemberTokenByMemberId(memberId) {
        try {
            const result = await MembersModel.findOne({_id: memberId});
            return result;
        } catch (err) {
            return err
        }
    }
    ,

    /**
     * this method get list of member id's and then return
     * @param {Array} phoneNumbers list of phone number
     */
    async getMemberTokenByPhoneNumber(phoneNumber) {
        try {
            const result = await MembersModel.findOne({phoneNumber}).sort({createdAt: 'desc'});
            return result;
        } catch (err) {
            return err
        }
    }
    ,
    /**
     * this method return list of members.
     * @return list of members
     */
    async getAllMembers(memberId, phoneNumber, nationalCode, fullName, createdAt, lastVisit, pageIndex = 1, pageSize = 10) {

        let query = {};
        if (memberId && memberId.toString().length > 0) {
            query._id = mongoose.Types.ObjectId(memberId);
        }
        if (phoneNumber && phoneNumber.toString().length > 0) {
            query.phoneNumber = phoneNumber;
        }
        if (nationalCode && nationalCode.toString().length > 0) {
            query.nationalCode = nationalCode;
        }
        if (fullName && fullName.toString().length > 0) {
            const regex = new RegExp(fullName, 'i')
            query.fullName = {$regex: regex};
        }
        if (createdAt && createdAt.toString().length > 0) {
            query.createdAt = {
                $gte: new Date(createdAt[0]),
                $lt: new Date(createdAt[1])
            };
        }
        if (lastVisit && lastVisit.toString().length > 0) {
            query.lastVisit = {
                $gte: new Date(lastVisit)
            };
        }

        return new Promise((resolve, reject) => {
            MembersModel.find(query)
                .skip(pageSize * (pageIndex - 1))
                .limit(pageSize)
                .sort({lastVisit: 'desc'})
                .then(r => resolve(r))
                .catch(er => reject(er));
        });
    }
    ,
    /**
     * This method get id of app and return list of members.
     * @param {String} appId   id of app
     */
    async getAllMembersByAppId(appId, memberId, phoneNumber, nationalCode, fullName, createdAt, lastVisit, pageIndex = 1, pageSize = 10) {

        let query = {
            apps: appId
        };
        if (memberId && memberId.toString().length > 0) {
            query._id = mongoose.Types.ObjectId(memberId);
        }
        if (phoneNumber && phoneNumber.toString().length > 0) {
            query.phoneNumber = phoneNumber;
        }
        if (nationalCode && nationalCode.toString().length > 0) {
            query.nationalCode = nationalCode;
        }
        if (fullName && fullName.toString().length > 0) {
            const regex = new RegExp(fullName, 'i')
            query.fullName = {$regex: regex};
        }
        if (createdAt && createdAt.toString().length > 0) {
            query.createdAt = {
                $gte: new Date(createdAt[0]),
                $lt: new Date(createdAt[1])
            };
        }
        if (lastVisit && lastVisit.toString().length > 0) {
            query.lastVisit = {
                $gte: new Date(lastVisit)
            };
        }

        return new Promise((resolve, reject) => {
            MembersModel.find(query)
                .skip(pageSize * (pageIndex - 1))
                .limit(pageSize)
                .sort({createdAt: 'desc'})
                .then(r => {
                    resolve(r)
                })
                .catch(er => {
                    reject(er)
                });
        });
    }
    ,
    /**
     * This method return installed count.
     */
    async getInstallStatistics(pageIndex = 1, pageSize = 10) {

        return new Promise((resolve, reject) => {
            MembersModel.aggregate([
                {
                    "$group": {
                        "_id": {
                            "$dateToString": {
                                "format": "%Y-%m-%d",
                                "date": "$createdAt"
                            }
                        },
                        "count": {
                            "$sum": 1
                        },
                        "activeCount": {
                            "$sum": {
                                "$cond": [
                                    "$isMobileVerified",
                                    1.0,
                                    0.0
                                ]
                            }
                        }
                    }
                },
                {
                    "$sort": {
                        "_id": -1
                    }
                },
                {
                    "$skip": pageSize * (pageIndex - 1)
                },
                {
                    "$limit": pageSize
                }
            ]).exec((err, statistics) => {
                if (err) throw reject(err);
                resolve(statistics);
            });
        });
    }
    ,
    /**
     * This method return count of installed
     * @param {String} appId   id of app
     */
    async getStatistics() {

        // return new Promise((resolve, reject) => {

        var count = {
            installedCount: 0,
            nonDuplicateInstalled: 0,
            activatedCount: 0,
        }

        try {

            count.installedCount = await MembersModel.count({})
            var nonDuplicatedMembers = await MembersModel.distinct('phoneNumber')
            count.nonDuplicateInstalled = nonDuplicatedMembers.length

            var activatedMembers = await MembersModel.find({isMobileVerified: true}).distinct('phoneNumber')
            count.activatedCount = activatedMembers.length

            return count
        } catch (e) {
            return e
        }

        // });
    }
    ,
    /**
     * This method return visits
     * @param {String} appId   id of app
     */
    async getVisits(phoneNumber, lastVisit, pageIndex = 1, pageSize = 10) {
        return new Promise((resolve, reject) => {

            let query = {
                lastVisit: new Date(new Date().getUTCFullYear(), new Date().getUTCMonth(), new Date().getUTCDay())
            };
            if (phoneNumber && phoneNumber.toString().length > 0) {
                query.phoneNumber = phoneNumber;
            }
            if (lastVisit && lastVisit.toString().length > 0) {
                query.lastVisit = {
                    $gte: new Date(lastVisit)
                };
            }

            console.log(query);

            MembersModel.aggregate([
                {
                    "$match": query
                },
                {
                    "$sort": {
                        "lastVisit": -1
                    }
                },
                {
                    "$group": {
                        "_id": "$phoneNumber",
                        "lastVisit": {
                            "$first": "$lastVisit"
                        }
                    }
                },
                {
                    "$skip": pageSize * (pageIndex - 1)
                },
                {
                    "$limit": pageSize
                }
            ]).exec((err, statistics) => {
                if (err) throw reject(err);
                resolve(statistics);
            });

        });
    }
    ,
    /**
     * This method return count of visits
     * @param {String} appId   id of app
     */
    async getVisitsCount(phoneNumber, lastVisit) {
        let query = {};
        if (phoneNumber && phoneNumber.toString().length > 0) {
            query.phoneNumber = phoneNumber;
        }
        if (lastVisit && lastVisit.toString().length > 0) {
            query.lastVisit = {
                $gte: new Date(lastVisit)
            };
        }

        const count = {
            visitsCount: 0
        }


        console.log(query);
        var activatedMembers = await MembersModel.find(query).distinct('phoneNumber')
        count.visitsCount = activatedMembers.length;

        return count;
    }
    ,
    /**
     * This method return count of members
     * @param {String}
     */
    async getMembersCount(memberId, phoneNumber, lastVisit) {
        let query = {};

        if (memberId && memberId.toString().length > 0) {
            query._id = mongoose.Types.ObjectId(memberId);
        }
        if (phoneNumber && phoneNumber.toString().length > 0) {
            query.phoneNumber = phoneNumber;
        }
        if (lastVisit && lastVisit.toString().length > 0) {
            query.lastVisit = {
                $gte: new Date(lastVisit)
            };
        }

        const count = {
            membersCount: 0
        }


        console.log(query);
        var activatedMembers = await MembersModel.find(query);
        count.membersCount = activatedMembers.length;

        return count;
    }
    ,
    /**
     * This method return members count by appId
     * @param {String} appId   id of app
     */
    async getMembersCountByAppId(appId, memberId, phoneNumber, lastVisit) {
        let query = {
            apps: appId
        };
        if (memberId && memberId.toString().length > 0) {
            query._id = mongoose.Types.ObjectId(memberId);
        }
        if (phoneNumber && phoneNumber.toString().length > 0) {
            query.phoneNumber = phoneNumber;
        }
        if (lastVisit && lastVisit.toString().length > 0) {
            query.lastVisit = {
                $gte: new Date(lastVisit)
            };
        }

        const count = {
            membersCount: 0
        }

        console.log(query);
        var activatedMembers = await MembersModel.find(query);
        count.membersCount = activatedMembers.length;

        return count;
    },
    activateMember(memberId, active) {

        const _id = mongoose.Types.ObjectId(memberId);
        const query = {"_id": _id};
        const changes = {"isActive": active};

        return MembersModel.findOneAndUpdate(query, changes, {
            upsert: true,
            new: true
        })
            .then(r => {
                return r;
            })
            .catch(err => {
                console.log(err);
            });
    },
};

module.exports = Members;