const {FilesController} = require("interact-api").API;
const express = require("express");
const ResponseDecorator = require("interact-response-decorator");
const router = express.Router();

/**
 * This method is upload new file.
 * @return (Object) file object.
 */
router.post("/upload", async (req, res) => {

    try {
        const {appId, memberId, fileName, orginalName, mimeType, size, fileUrl} = req.body;

        console.log(appId);

        FilesController.create(appId, memberId, fileName, orginalName, mimeType, size, fileUrl)
            .then(result => {
                res.json(result);
            })
            .catch(err => {
                res.json(err);
            });
    } catch (e) {
        console.log(e);
    }
});

/**
 * This method gets file.
 * @return (Object) file object.
 */
router.post('/getFile', (req, res) => {

    const {fileId} = req.body;

    FilesController.findById(fileId).then(result => {
        const response = ResponseDecorator.success(result);
        res.json(response);
    }).catch(err => {
        const response = ResponseDecorator.dbFailed(err.message);
        res.json(response);
    });
});

/**
 * This method gets all files.
 * @return (Object) file object.
 */
router.post('/getFiles', (req, res) => {

    const {appId, pageIndex, pageSize} = req.body;

    FilesController.findByQuery(appId, parseInt(pageIndex), parseInt(pageSize)).then(result => {
        const response = ResponseDecorator.success(result);
        res.json(response);
    }).catch(err => {
        const response = ResponseDecorator.dbFailed(err.message);
        res.json(response);
    });
});

/**
 * This method get an id of service and then delete service
 * @param {String|ObjectId} serviceId id of service that you want ot delete it
 */
router.post("/delete", (req, res) => {
    FilesController.remove(req.body.fileId)
        .then(result => {
            res.send(result);
        })
        .catch(err => {
            console.log(err);
        });
});

module.exports = router;
