const {
    MembersController,
    MemberActivationController,
    VersionsController,
    ActivityController
} = require("interact-api").API;
const express = require("express");
const ResponseDecorator = require("interact-response-decorator");
const router = express.Router();
const jwt = require("jsonwebtoken");
const MailMan = require("mailman");
const Messages = require("../../config/Messages");
const config = require("config");
const axios = require('axios').default;
const TAG = `${this} `;
const ActivityStatus = require("../../helper/ActivityStatus");
const URL = require("../../helper/URL");

const {CheckESCOEmployees} = require("../../helper/CheckEmployee");

/**
 * This method get member id and return all information of member.
 * @param {String} _id id of member
 */
router.post("/getMember", (req, res) => {
    const {memberId} = req.body;

    MembersController.getById(memberId).then(result => {
        const response = ResponseDecorator.success(result);
        res.json(response);
    }).catch(err => {
        const response = ResponseDecorator.dbFailed(err.message);
        res.json(response);
    });
});

/**
 * this method get appAddress and memberId and join member to the app
 * @param {String} appAddress address of app
 * @param {String} memberId id of member
 */
router.post("/join", (req, res) => {
    const {memberId} = req.user;
    const {appAddress} = req.body;
    MembersController.join(appAddress, memberId)
        .then(result => {
            const response = ResponseDecorator.success(result);
            res.json(response);
        })
        .catch(err => {
            if (err.msg) {
                const response = ResponseDecorator.failWithResponse(err.msg, [
                    {versionId: err.versionId}
                ]);
                res.json(response);
            } else {
                const response = ResponseDecorator.fail(err);
                res.json(response);
            }
        });
});


/**
 * this method get appAddress and memberId and join member to the app
 * @param {String} appAddress address of app
 * @param {String} memberId id of member
 */
router.post("/joinWithMobile", (req, res) => {
    const {phoneNumbers} = req.body;
    const {appId} = req.body;
    MembersController.joinWithMobile(appId, phoneNumbers)
        .then(result => {
            const response = ResponseDecorator.success(result);
            res.json(response);
        })
        .catch(err => {
            if (err.msg) {
                const response = ResponseDecorator.failWithResponse(err.msg, [
                    {versionId: err.versionId}
                ]);
                res.json(response);
            } else {
                const response = ResponseDecorator.fail(err);
                res.json(response);
            }
        });
});

/**
 * this method get id of app and id of member and remove member from app
 * @param {String} appId id of app
 * @param {String} memberId id of member
 */
router.post("/leave", async (req, res) => {
    let {memberId} = req.user;
    const {appId} = req.body;

    if (!memberId) {
        memberId = req.body.memberId;
    }

    const member = await MembersController.getById(memberId);

    MembersController.leave(appId, memberId)
        .then(result => {

            //Save activity
            ActivityController.create(memberId, member.phoneNumber,
                "Leave Member",
                `Member left the App: ${appId} . Member: ${memberId}  Result: ${JSON.stringify(result)}`,
                URL.BASE_URL + req.originalUrl,
                ActivityStatus.INFO).then(r => console.log(r));

            const response = ResponseDecorator.success(result);
            res.send(response);
        })
        .catch(err => {

            //Save activity
            ActivityController.create(memberId, member.phoneNumber,
                "Leave Member",
                `An error occurred while leaving the Member. Member: ${memberId} App: ${appId}  Error: ${JSON.stringify(err)}`,
                URL.BASE_URL + req.originalUrl,
                ActivityStatus.INFO).then(r => console.log(r));

            const response = ResponseDecorator.fail(err);
            res.send(response);
        });
});

/**
 * this method get id of member and remove member
 * @param {String} memberId id of member
 */
router.post("/deleteMember", async (req, res) => {
    let {memberId} = req.user;

    if (!memberId) {
        memberId = req.body.memberId;
    }

    const member = await MembersController.getById(memberId);

    MembersController.delete(memberId)
        .then(result => {

            //Save activity
            ActivityController.create(memberId, member.phoneNumber,
                "Delete Member",
                `Member was removed successfully. Member: ${JSON.stringify(member)}`,
                URL.BASE_URL + req.originalUrl,
                ActivityStatus.INFO).then(r => console.log(r));

            const response = ResponseDecorator.success(result);
            res.send(response);
        })
        .catch(err => {

            //Save activity
            ActivityController.create(memberId, member.phoneNumber,
                "Delete Member",
                `Member deletion failed. Member: ${JSON.stringify(member)}  Error: ${JSON.stringify(err)}`,
                URL.BASE_URL + req.originalUrl,
                ActivityStatus.INFO).then(r => console.log(r));

            const response = ResponseDecorator.fail(err);
            res.send(response);
        });
});
/**
 * this method get member id and return all apps of this member
 * @param {String} memberId  id of member
 * @param {Array} apps list of apps of user
 */
router.post("/getApps", (req, res) => {
    const {memberId, phoneNumber} = req.user;
    const {apps, outtype} = req.body;
    const testMode = parseInt(req.get("d"), 10) === 1 ? true : false; //checking is member need test versions
    const outPutType = outtype ? outtype : "xml";
    let appList = [];
    if (apps === "[]") {
        appList = [];
    } else if (typeof apps === "string") {
        appList = JSON.parse(apps);
    } else {
        appList = apps;
    }


    let cdnUrl = null;
    let appoIcon = null;
    const baseUrl = config.get("SimpleResponseServer.environments.baseUrlLink");
    if (config.get("SimpleResponseServer.environments.type") === "aws") {
        cdnUrl = config.get("SimpleResponseServer.Storage.aws.cdn.bucketLink");
        appoIcon = config.get("SimpleResponseServer.Storage.aws.appoIcon");
    } else if (config.get("SimpleResponseServer.environments.type") === "local") {
        cdnUrl = config.get("Static_Resource.baseUrlLink");
        appoIcon = config.get("SimpleResponseServer.Storage.local.appoIcon");
    }

    MembersController.getApps(memberId, appList, outPutType)
        .then(result => {

            if (testMode) {
                //find all version in test mode that this member is registerd for it
                VersionsController.getTestVersions(memberId, outPutType)
                    .then(ts => {
                        //merge app and test apps in to result array
                        const response = ResponseDecorator.success(result.concat(ts));
                        res.json(response);

                        //Save activity
                        ActivityController.create(memberId, phoneNumber,
                            "Get Apps",
                            "Test mode is on. Get all test apps and merge with deploy apps.",
                            baseUrl + req.originalUrl,
                            ActivityStatus.INFO).then(r => console.log(r));
                    })
                    .catch(er => {

                        console.log(`er ==>${er}`);
                        const response = ResponseDecorator.fail(er);

                        //Save activity
                        ActivityController.create(memberId, phoneNumber,
                            "Get Apps",
                            "Test mode is on. Get test apps has error. " + er,
                            baseUrl + req.originalUrl,
                            ActivityStatus.ERROR).then(r => console.log(r));
                    });
            } else {
                const response = ResponseDecorator.success(result);
                res.json(response);


                //Save activity
                ActivityController.create(memberId, phoneNumber,
                    "Get Apps",
                    "Member got all deploy apps.",
                    baseUrl + req.originalUrl,
                    ActivityStatus.INFO
                ).then(r => console.log(r));
            }


            //Verify member with memberId
            const isMobileVerified = true;
            const lastVisit = Date.now();
            MembersController.update(memberId, null, {isMobileVerified, lastVisit})
                .then(member => {
                    console.log("Member verified");

                    //Save activity
                    ActivityController.create(memberId, phoneNumber,
                        "Get Apps",
                        "Member verified.",
                        baseUrl + req.originalUrl,
                        ActivityStatus.INFO).then(r => console.log(r));
                })
                .catch(e => {
                    console.log(e);

                    //Save activity
                    ActivityController.create(memberId, phoneNumber,
                        "Get Apps",
                        "Member not verified. " + e,
                        baseUrl + req.originalUrl,
                        ActivityStatus.ERROR).then(r => console.log(r));
                });
        })
        .catch(err => {
            const response = ResponseDecorator.fail(err);
            res.json(response);

            //Save activity
            ActivityController.create(memberId, phoneNumber,
                "Get Apps",
                err,
                baseUrl + req.originalUrl,
                ActivityStatus.ERROR).then(r => console.log(r));
        });
});

/**
 * this method get mobileNo and register user
 * @param {String} mobileNo mobile number of member that you want to register
 */
router.post("/registerWhiteoutVerify", (req, res) => {
    const {fullname, mobile, nationalcode, uniqueId, deviceInfo, registerationFCMToken, otp} = req.body;

    const fullName = fullname;
    const phoneNumber = mobile;
    const nationalCode = nationalcode;

    MembersController.registerWhiteoutVerify(fullName, phoneNumber, nationalCode, otp, uniqueId, deviceInfo, registerationFCMToken)
        .then(result => {
            const response = ResponseDecorator.success(result);
            res.send(response);
        }).catch(err => {
        const response = ResponseDecorator.fail(err);
        res.send(response);
    });
});

/**
 * this method get mobileNo and register user
 * @param {String} mobileNo mobile number of member that you want to register
 */
router.post("/register", (req, res) => {
    const {fullname, mobile, nationalcode, uniqueId, deviceInfo, registerationFCMToken} = req.body;
    let otp = Math.floor(1000 + Math.random() * 9000);
    let phoneNumber = mobile;
    let fullName = fullname;
    let nationalCode = nationalcode;
    const baseUrl = config.get("SimpleResponseServer.environments.baseUrlLink");

    MembersController.register(fullName, phoneNumber, nationalCode, otp, uniqueId, deviceInfo, registerationFCMToken)
        .then(result => {
            if (phoneNumber) {

                //Save activity
                ActivityController.create(result.memberId, phoneNumber,
                    "Register",
                    "Register is successfully.",
                    baseUrl + req.originalUrl,
                    ActivityStatus.INFO).then(r => console.log(r));

                sendSMS(phoneNumber, otp)
                    .then(r => {

                        //create a token with json web token
                        const token = jwt.sign(result, config.get("SimpleResponseServer.Security.jwtSecretKey"));
                        result["token"] = token;

                        const response = ResponseDecorator.success(result);
                        res.send(response);

                        //Save activity
                        ActivityController.create(result.memberId, phoneNumber,
                            "Register",
                            "OTP code sent successfully.",
                            baseUrl + req.originalUrl,
                            ActivityStatus.INFO).then(r => console.log(r));

                    })
                    .catch(error => {
                        console.log("Error  " + error);

                        //Save activity
                        ActivityController.create(result.memberId, phoneNumber,
                            "Register",
                            "Sending otp code has error. Error: " + error,
                            baseUrl + req.originalUrl,
                            ActivityStatus.ERROR).then(r => console.log(r));
                    });
            } else {
                const response = ResponseDecorator.success(result);
                res.send(response);


                //Save activity
                ActivityController.create(result.memberId, phoneNumber,
                    "Register",
                    "The register failed. Info: phoneNumber is undefined.",
                    baseUrl + req.originalUrl,
                    ActivityStatus.INFO).then(r => console.log(r));
            }
        })
        .catch(err => {
            console.log(err);
            const response = ResponseDecorator.fail(err);
            res.send(response);

            //Save activity
            ActivityController.create(null, phoneNumber,
                "Register",
                "The register failed. Error: " + err,
                baseUrl + req.originalUrl,
                ActivityStatus.ERROR).then(r => console.log(r));
        });
});

router.post("/verifyMember", async (req, res) => {

    //get headers and body from user
    const {mobile, otp, uniqueId} = req.body;
    let phoneNumber = mobile;
    const member = await MembersController.getByUniqueId(uniqueId);
    const memberId = member.id;


    /**
     * Checking member
     * If member isn't employee in organization he can't activate.
     */
    let appName = config.get('Organization.applicationNameEn');
    if (appName === "EsfahanSteel") {

        const employee = await CheckESCOEmployees(phoneNumber);
        if (employee) {

            //Save activity
            ActivityController.create(memberId, phoneNumber,
                "Verify member",
                `This user is employee of organization. Result: ${JSON.stringify(employee)}`,
                URL.BASE_URL + req.originalUrl,
                ActivityStatus.INFO).then(r => console.log(r));

        } else {

            const response = ResponseDecorator.fail("You aren't user of organization.");
            res.send(response);

            //Save activity
            ActivityController.create(memberId, phoneNumber,
                "Verify member",
                "This user isn't employee of organization.",
                URL.BASE_URL + req.originalUrl,
                ActivityStatus.INFO).then(r => console.log(r));

            return;
        }
    }

    MembersController.verifyMember(phoneNumber, otp, uniqueId)
        .then(result => {
            if (result == 0) {
                const response = ResponseDecorator.fail("Invalid verification code.");
                res.send(response);

                //Save activity
                ActivityController.create(memberId, phoneNumber,
                    "Verify member",
                    `Invalid verification code of ${otp}. Error: ${result}`,
                    URL.BASE_URL + req.originalUrl,
                    ActivityStatus.ERROR).then(r => console.log(r));

            } else {

                //Save activity
                ActivityController.create(memberId, phoneNumber,
                    "Verify member",
                    `The verification code of ${otp} was successful. Info: ${JSON.stringify(result)}`,
                    URL.BASE_URL + req.originalUrl,
                    ActivityStatus.INFO).then(r => console.log(r));


                const isMobileVerified = true;
                MembersController.update(null, uniqueId, {isMobileVerified})
                    .then(member => {

                        const token = jwt.sign(result, config.get("SimpleResponseServer.Security.jwtSecretKey"));
                        member["token"] = token;
                        const response = ResponseDecorator.success(member);
                        res.send(response);


                        //Save activity
                        ActivityController.create(memberId, phoneNumber,
                            "Verify member",
                            `Update mobileVerified to true.`,
                            URL.BASE_URL + req.originalUrl,
                            ActivityStatus.INFO).then(r => console.log(r));

                    })
                    .catch(e => {
                        console.log(e);
                        const response = ResponseDecorator.fail("Invalid verification code.");
                        res.send(response);

                        //Save activity
                        ActivityController.create(memberId, phoneNumber,
                            "Verify member",
                            `Error in update mobileVerified. Error: ${e}`,
                            URL.BASE_URL + req.originalUrl,
                            ActivityStatus.ERROR).then(r => console.log(r));
                    });
            }
        })
        .catch(error => {
            const response = ResponseDecorator.fail("Invalid verification code.");
            res.send(response);

            //Save activity
            ActivityController.create(memberId, phoneNumber,
                "Verify member",
                `Error in verification member. Error: ${error}`,
                URL.BASE_URL + req.originalUrl,
                ActivityStatus.ERROR).then(r => console.log(r));
        })
});

async function sendSMS(mobile, otp) {

    let smsUrl = config.get('OTP.url');
    let hashcode = config.get('OTP.appLicationSignature');
    let data = {};
    let appName = config.get('Organization.applicationNameEn');
    let options = {
        "headers": {
            'Content-Type': 'application/json',
        },
    };

    let response = null;
    if (appName === 'CPay' || appName === 'Tafrihati' || appName === 'Sita' || appName === 'SitaTest'
        || appName === 'Kanoon' || appName === 'SitaRC' || appName === 'ParsianKishSupport') {
        data = {
            "mobile": mobile,
            "msg": getMessage(appName, otp, hashcode),
            "token": config.get('OTP.token')
        };
        options = {
            "method": "post",
            "url": smsUrl,
            "data": data,
            "headers": {
                'Content-Type': 'application/json',
            },
        }
    }

    if (appName === 'EsfahanSteel') {
        smsUrl += `?from=30008723000044&to=${mobile}&username=zobahanuser10&password=*6k52qa8s~o93&text=${getMessage(appName, otp)}`;
        smsUrl = encodeURI(smsUrl);

        options = {
            "method": "get",
            "url": smsUrl,
            "headers": {
                'Content-Type': 'application/soap+xml;',
            },
        }
    }


    if (appName === 'sums') {
        const data = {
            "organization": "sums",
            "username": "appogram",
            "password": "Sumsapp@1967",
            "method": "send",
            "messages": [
                {
                    "sender": "982000127010",
                    "recipient": mobile,
                    "body": getMessage(appName, otp),
                    "customerId": 1
                }
            ]
        };
        options = {
            "method": "post",
            "url": smsUrl,
            "data": data,
            "headers": {
                'Content-Type': 'application/json;',
            },
        }
    }


    try {
        response = await axios(options);
        console.log(response.data);
        return response.data;
        res.status(200);
        res.send(response.data)
    } catch (error) {
        console.log(error.message);
        return error;
        res.status(500);
        res.send(error.message)
    }

}

function getMessage(appName, otp, hashcode) {
    let msg = `کدفعالسازی:${otp}`; //default message
    if (appName === 'Tafrihati') {
        msg = `<#>\nکدفعالسازی:${otp}\n ${config.get('Organization.applicationNameFa')}\n\n${hashcode}`;
    }
    if (appName === 'CPAY') {
        msg = `<#>\nکدفعالسازی:${otp}\n${config.get('Organization.applicationNameFa')}\n\n${hashcode}`;
    }
    if (appName === 'Sita') {
        msg = `<#>\nکدفعالسازی:${otp}\n${config.get('Organization.applicationNameFa')}\n\n${hashcode}`;
    }
    if (appName === 'SitaTest') {
        msg = `<#>\nکدفعالسازی:${otp}\n${config.get('Organization.applicationNameFa')}\n\n${hashcode}`;
    }
    if (appName === 'Kanoon') {
        msg = `<#>\nکدفعالسازی:${otp}\n${config.get('Organization.applicationNameFa')}\n\n${hashcode}`;
    }
    if (appName === 'SitaRC') {
        msg = `<#>\nکدفعالسازی:${otp}\n${config.get('Organization.applicationNameFa')}\n\n${hashcode}`;
    }
    if (appName === 'ParsianKishSupport') {
        msg = `<#>\nکدفعالسازی:${otp}\n${config.get('Organization.applicationNameFa')}\n\n${hashcode}`;
    }
    if (appName === 'EsfahanSteel') {
        msg = `کدفعالسازی${otp}`;
    }
    if (appName === 'sums') {
        msg = `<#>\nکدفعالسازی:${otp}\n${config.get('Organization.applicationNameFa')}\n\n${hashcode}`;
    }
    return msg;
}

/**
 * This method get member by memberId and update information of member.
 * @param {String} memberId id of member
 * @param {String} fullName full name of member
 * @param {String} phoneNumber mobile number of member
 * @param {String} nationalCode national code of member
 * @param {String} deviceInfo information of device.
 */
router.post("/update", (req, res) => {
    const {memberId, uniqueId} = req.user;//filter
    const {fullName, phoneNumber, nationalCode, deviceInfo} = req.body;  //changes

    MembersController.update(memberId, uniqueId, {uniqueId, fullName, phoneNumber, nationalCode, deviceInfo})
        .then(result => {
            const response = ResponseDecorator.success(result);
            res.send(response);
        }).catch(err => {
        const response = ResponseDecorator.fail(err);
        res.send(response);
    });
});
/**
 * this route get unique id and generate new json web token and set it on the header
 * and then send new jwt to member
 */
router.post("/renewtoken", (req, res) => {
    const {uniqueId} = req.body;
    MembersController.getBasicInfo(uniqueId)
        .then(result => {
            if (result === null) {
                const response = ResponseDecorator.fail(Messages.DEVICE_INVALID);
                res.json(response);
            }
            const token = jwt.sign(
                result,
                config.get("SimpleResponseServer.Security.jwtSecretKey")
            );
            const r = {
                email: result.email,
                token
            };
            const response = ResponseDecorator.success(r);
            res.json(response);
        })
        .catch(er => {
            const response = ResponseDecorator.fail(er);
            res.json(response);
        });
});
/**
 * this method get member id and FCM token and set token to member with that id
 */
router.post("/setFCMToken", (req, res) => {
    const {memberId} = req.user;
    const {token} = req.body;
    MembersController.setRegisterationFCMToken(memberId, token)
        .then(r => res.json(ResponseDecorator.success(r)))
        .catch(err => res.json(ResponseDecorator.fail(err)));
});

router.post('/setSocketToken', async (req, res) => {
    const {memberId} = req.user;
    const {socketToken} = req.body;
    try {
        const result = await MembersController.setSocketToken(memberId, socketToken);
        if (result) {
            res.json(ResponseDecorator.successWithoutResponse());
        } else {
            res.json(ResponseDecorator.failWithoutResponse());
        }
    } catch (err) {
        res.json(ResponseDecorator.failWithoutResponse())
    }

})
/**
 * this method get email and send a verification code to email
 */
router.post("/sendverificationcode", (req, res) => {
    const {email} = req.body;
    const uId = shortNumberId();
    MemberActivationController.create(email, uId)
        .then(result => {
            const {code} = result;
            const from = "Appogram <notification@appogram.io>";
            const to = email;
            const subject = "Verification Code";
            const text = `Your verification code is ${code}`;
            const html = `Your verification code is <b>${code}</b>`;
            MailMan.send(from, to, subject, text, html)
                .then(r => res.send(r))
                .catch(err => res.send(r));
            const response = ResponseDecorator.successWithoutResponse();
            res.json(response);
        })
        .catch(err => {
            const response = ResponseDecorator.fail(err);
            res.send(response);
        });
});

/**
 * logout user form app
 * @param {String} uniqueId unique id of member
 */
router.post("/logout", (req, res) => {
    const {uniqueId} = req.body;
    MembersController.logout(uniqueId)
        .then(result => {
            const response =
                result === true ?
                    ResponseDecorator.successWithoutResponse() :
                    ResponseDecorator.fail("");
            res.json(response);
        })
        .catch(err => {
            const response = ResponseDecorator.fail("");
            res.json(response);
        });
});

router.post("/connectaccount", (req, res) => {
    const {uniqueId, email, verification, deviceInfo} = req.body;
    MembersController.connectAccount(uniqueId, email, verification)
        .then(result => {
            const token = jwt.sign(result, jwtSecretKey);
            result["token"] = token;
            const response = ResponseDecorator.success(result);
            res.json(response);
        })
        .catch(err => {
            const response = ResponseDecorator.fail(err);
            res.json(response);
        });
});

router.post("/addToTest", (req, res) => {
    const {versionHash} = req.body;
    const {memberId} = req.user;
    VersionsController.addMemberToVersionByHash(versionHash, memberId)
        .then(result => {
            //it's return version
            const response = ResponseDecorator.successWithoutResponse();
            res.json(response);
        })
        .catch(er => {
            if (er.msg) {
                const response = ResponseDecorator.failWithResponse(er.msg, [
                    {versionId: er.versionId}
                ]);
                res.json(response);
            } else {
                const response = ResponseDecorator.fail(er);
            }
        });
});

router.post("/removeFromTest", (req, res) => {
    const {versionId} = req.body;
    const {memberId} = req.user;
    VersionsController.removeMemberFromVersionById(versionId, memberId)
        .then(result => {
            //it's return version
            const response = ResponseDecorator.successWithoutResponse();
            res.json(response);
        })
        .catch(er => {
            const response = ResponseDecorator.fail(err);
            res.json(response);
        });
});

router.post("/activateMember", (req, res) => {
    const {memberId, active} = req.body;

    MembersController.activateMember(memberId, active)
        .then(result => {
            //it's return version
            const response = ResponseDecorator.success(result);
            res.json(response);
        })
        .catch(er => {
            const response = ResponseDecorator.fail(er);
            res.json(response);
        });
});

/**
 * simple function for create basic uuid
 */
const shortNumberId = () => {
    const startFragment = Math.round(Math.random() * 4);
    const idLenght = 4;
    const endFragment = startFragment + idLenght;
    const n = Math.round(Math.random() * 9999999999999999999) + "";
    const uniqueId = parseInt(n.slice(startFragment, endFragment), 10);
    return uniqueId;
};

module.exports = router;