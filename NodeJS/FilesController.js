/**
 * LogsController manage many things about logs in application.
 */

const FileModel = require('../Models/FileModel')

const FileController = {

    /**
     * this method get applicaiton id and id of member and version id and ip address of member and also log message and create
     * a new log
     * @param {ObjectId} appId id of app
     * @param {ObjectId} memberId id of member
     * @param {ObjectId} versionId  id of version
     * @param {String} log  log contents
     * @param {String} ip ip address of member
     * @returns if successfull return an object that contain Logs Model Schema {_id,appId,memberId,versionId,log,ip}
     */

    async create(appId, memberId, fileName, originalName, mimeType, size, fileUrl) {

        const newFile = new FileModel({appId, memberId, fileName, originalName, mimeType, size, fileUrl});

        return newFile.save().then((result) => {
            return result;
        }).catch((err) => {
            return err;
        })
    },
    /**
     * this method get an appId and return all applogs related to this appId
     * @param {String} appId app id that you want to get all logs of it
     */
    findById(id) {

        return new Promise((resolve, reject) => {
            FileModel.findOne({_id: id})
                .then(r => resolve(r))
                .catch(er => {
                    console.log(er);
                    reject(er)
                });
        });
    },
    /**
     * this method get an appId and return all applogs related to this appId
     * @param {String} appId app id that you want to get all logs of it
     */
    findByQuery(appId, pageIndex = 1, pageSize = 10) {

        return new Promise((resolve, reject) => {
            FileModel.find({appId})
                .skip(pageSize * (pageIndex - 1))
                .limit(pageSize)
                .sort({createdAt: 'desc'})
                .then(r => resolve(r))
                .catch(er => {
                    console.log(er);
                    reject(er)
                });
        });
    },
    remove(id) {
        return FileModel.remove({_id: id}).then(result => {
            return result;
        }).catch(err => {
            return err;
        })
    },
};

module.exports = FileController;